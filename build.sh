#!/usr/bin/env bash
#
################################################################
# Created By: Alex M. Schapelle
# Purpose: build script to build books and mini-books
# Date: 23/06/2022S
# Version: 0.1.0
################################################################

PROJECT=

function main(){

    check_dependencies

}


function check_dependencies(){
    
    if [ -e $(which pipenv) ];then
        export BUILDER=$(which pipenv)
    else
        echo "[!] Dependency missing: please install python3 and pipenv"
        return 1
    fi
}

function generate_artifact(){
    find $PROJECT
}

function setup_build_environment(){
    pipenv run make.py #book or minibook name needs to be provided
}



#######
# Main: DO NOT REMOVE
#######
main
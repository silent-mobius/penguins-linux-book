---

# package terminology 
# repository 
A lot of software and documentation for your Linux distribution is available as `packages` in one or more centrally distributed `repositories`  repository  . These `packages` in such a `repository` are tested and very easy to install (or remove) with a graphical or command line installer. 


# .deb packages 
Debian, Ubuntu, Mint and all derivatives from Debian and Ubuntu use `.deb`  deb(5)   packages. To manage software on these systems, you can use `aptitude`  aptitude(8)   or `apt-get`  apt-get(8)  , both these tools are a front end for `dpkg`  dpkg(8)  . 


# .rpm packages 
Red Hat, Fedora, CentOS, OpenSUSE, Mandriva, Red Flag and others use `.rpm`  rpm(8)   packages. The tools to manage software packages on these systems are `yum` and `rpm`. 


# dependency 
Some packages need other packages to function. Tools like `apt-get`, `aptitude` and `yum` will install all `dependencies` you need. When using `dpkg` or `rpm`, or when building from `source`, you will need to install dependencies yourself. 


# open source 
These repositories contain a lot of independent `open source software`. Often the source code is customized to integrate better with your distribution. Most distributions also offer this modified source code as a `package` in one or more `source repositories`. 
You are free to go to the project website itself (samba.org, apache.org, github.com, ...) an download the `vanilla`  vanilla   (= without the custom distribution changes) source code. 


---

# GUI software management 
End users have several graphical applications available via the desktop (look for 'add/remove software' or something similar). 
Below a screenshot of Ubuntu Software Center running on Ubuntu 12.04. Graphical tools are not discussed in this book. 

<img  src="img/ubuntu_software_center.png" format="EPS" align="center"> </img> 			






---

# security based on user name 
# valid users 
To restrict users per share, you can use the `valid users`  valid users (Samba)   parameter. In the example below, only the users listed as valid will be able to access the tennis share. 
```sh
[tennis]
path = /srv/samba/tennis
comment = authenticated and valid users only
read only = No
guest ok = No
valid users = serena, kim, venus, justine ```


# invalid users 
If you are paranoia, you can also use `invalid users`  invalid users (Samba)   to explicitely deny the listed users access. When a user is in both lists, the user has no access! 
```sh
[tennis]
path = /srv/samba/tennis
read only = No
guest ok = No
valid users = kim, serena, venus, justine
invalid users = venus```


# read list 
On a writable share, you can set a list of read only users with the `read list`  read list (Samba)   parameter. 
```sh
[football]
path = /srv/samba/football
read only = No
guest ok = No
read list = martina, roberto```


# write list 
Even on a read only share, you can set a list of users that can write. Use the `write list`  write list (Samba)   parameter. 
```sh
[football]
path = /srv/samba/golf
read only = Yes
guest ok = No
write list = eddy, jan```




# security based on ip-address 
# hosts allow 
The `hosts allow`  hosts allow (Samba)   or `allow hosts`  allow hosts (Samba)   parameter is one of the key advantages of Samba. It allows access control of shares on the ip-address level. To allow only specific hosts to access a share, list the hosts, separated by comma's. 
```sh
allow hosts = 192.168.1.5, 192.168.1.40```
Allowing entire subnets is done by ending the range with a dot. 
```sh
allow hosts = 192.168.1.```
Subnet masks can be added in the classical way. 
```sh
allow hosts = 10.0.0.0/255.0.0.0```
You can also allow an entire subnet with exceptions. 
```sh
hosts allow = 10. except 10.0.0.12```


# hosts deny 
The `hosts deny`  hosts deny (Samba)   or `deny hosts`  deny hosts (Samba)   parameter is the logical counterpart of the previous. The syntax is the same as for hosts allow. 
```sh
hosts deny = 192.168.1.55, 192.168.1.56```




# security through obscurity 
# hide unreadable 
Setting `hide unreadable`  hide unreadable (Samba)   to yes will prevent users from seeing files that cannot be read by them. 
```sh
hide unreadable = yes```


# browsable 
Setting the `browseable = no`  browseable (Samba)   directive will hide shares from My Network Places. But it will not prevent someone from accessing the share (when the name of the share is known). 
Note that `browsable`  browsable (Samba)   and `browseable` are both correct syntax. 
```sh
[pubread]
path = /srv/samba/readonly
comment = files to read
read only = yes
guest ok = yes
browseable = no```




# file system security 
# create mask 
You can use `create mask`  create mask (Samba)   and `directory mask`  directory mask (Samba)   to set the maximum allowed permissions for newly created files and directories. The mask you set is an AND mask (it takes permissions away). 
```sh
[tennis]
path = /srv/samba/tennis
read only = No
guest ok = No
create mask = 640
directory mask = 750```


# force create mode 
Similar to `create mask`, but different. Where the mask from above was a logical AND, the mode you set here is a logical OR (so it adds permissions). You can use the `force create mode`  force create mode(samba)   and `force directory mode`  force directory mode(samba)   to set the minimal required permissions for newly created files and directories. 
```sh
[tennis]
path = /srv/samba/tennis
read only = No
guest ok = No
force create mode = 444
force directory mode = 550```


# security mask 
The `security mask`  security mask(samba)   and `directory security mask`  directory security mask(samba)   work in the same way as `create mask` and `directory mask`, but apply only when a windows user is changing permissions using the windows security dialog box. 


# force security mode 
The `force security mode`  force security mode(samba)   and `force directory security mode`  force directory security mode(samba)   work in the same way as `force create mode` and `force directory mode`, but apply only when a windows user is changing permissions using the windows security dialog box. 


# inherit permissions 
With `inherit permissions = yes` you can force newly created files and directories to inherit permissions from their parent directory, overriding the create mask and directory mask settings. 
```sh
[authwrite]
path = /srv/samba/authwrite
comment = authenticated users only
read only = no  
guest ok = no   
create mask = 600
directory mask = 555
inherit permissions = yes```






---

# Binary subnets 
Computers count `bits` bit and` bytes` byte, humans count decimal. Our computers see an `ip address` ip address as a 32-bit number, the same goes for the` subnet mask` subnet mask. The three subnet masks we know so far are decimal and binary represented in the following table:
<table frame='all'>## binary classful  classful   subnets 
<?dbfo table-width="90%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="2*" align="center"/>
<colspec colname='c3' colwidth="6*" align="center"/>
<thead>
<row>
<entry>class</entry>
<entry>subnet</entry>
<entry>binary subnet</entry>
</row>
</thead>
<tbody>
<row>
<entry>A</entry>
<entry>255.0.0.0</entry>
<entry>11111111.00000000.00000000.00000000</entry>
</row>
<row>
<entry>B</entry>
<entry>255.255.0.0</entry>
<entry>11111111.11111111.00000000.00000000</entry>
</row>
<row>
<entry>C</entry>
<entry>255.255.255.0</entry>
<entry>11111111.11111111.11111111.00000000</entry>
</row>
</tbody>
</tgroup>
</table>
Voor de volledigheid volgt ook nog eens het aantal computers dat in een `classful` netwerk past. 
<table frame='all'>## max computers classful subnets 
<?dbfo table-width="90%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="6*" align="center"/>
<colspec colname='c3' colwidth="3*" align="center"/>
<thead>
<row>
<entry>class</entry>
<entry>binary subnet</entry>
<entry>computers</entry>
</row>
</thead>
<tbody>
<row>
<entry>A</entry>
<entry>11111111.00000000.00000000.00000000</entry>
<entry>256*256*256 - 2</entry>
</row>
<row>
<entry>B</entry>
<entry>11111111.11111111.00000000.00000000</entry>
<entry>256*256 - 2</entry>
</row>
<row>
<entry>C</entry>
<entry>11111111.11111111.11111111.00000000</entry>
<entry>256 - 2</entry>
</row>
</tbody>
</tgroup>
</table>


---

# supernetting 
Zoals je ziet, begint de subnet mask binair steeds met eentjes en eindigt steeds met nulletjes. Wat als we nu een eentje meer of minder zetten ? 
Onze verkorte notatie  cidr   laat toe om dit snel te noteren. We schrijven simpelweg 172.16.0.0/17 i.p.v. 172.16.0.0/16 . Maar wat zijn de gevolgen hiervan ? 
Ten eerste is een eentje meer hetzelfde als het halveren van het aantal computers in het netwerk. Want we vergroten het `network id`  network id   en verkleinen het aantal mogelijke `host id's`  host id  . 
Laten we ons voorbeeld beginnen met het netwerk in de klas zoals het er nu uitziet: 

<table frame='all'>## /24 netwerk in de klas 
<?dbfo table-width="99%" ?>
<tgroup cols='4' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="center"/>
<colspec colname='c2' colwidth="1*" align="center"/>
<colspec colname='c3' colwidth="6*" align="center"/>
<colspec colname='c4' colwidth="2*" align="center"/>
<thead>
<row>
<entry>network id</entry>
<entry>subnet</entry>
<entry>binair subnet</entry>
<entry>aantal computers</entry>
</row>
</thead>
<tbody>
<row>
<entry>192.168.0.0</entry>
<entry>/24</entry>
<entry>11111111.11111111.11111111.00000000</entry>
<entry>256 - 2 = 254</entry>
</row>
</tbody>
</tgroup>
</table>
En kijk wat er gebeurt als we de `subnet mask`  subnet mask   veranderen van 24 `bits` naar 25 `bits`  bit   die op 1 staan. 
<table frame='all'>## /25 netwerk in de klas 
<?dbfo table-width="99%" ?>
<tgroup cols='4' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="center"/>
<colspec colname='c2' colwidth="1*" align="center"/>
<colspec colname='c3' colwidth="6*" align="center"/>
<colspec colname='c4' colwidth="2*" align="center"/>
<thead>
<row>
<entry>network id</entry>
<entry>subnet</entry>
<entry>binair subnet</entry>
<entry>aantal computers</entry>
</row>
</thead>
<tbody>
<row>
<entry>192.168.0.0</entry>
<entry>/25</entry>
<entry>11111111.11111111.11111111.10000000</entry>
<entry>128 - 2 = 126</entry>
</row>
</tbody>
</tgroup>
</table>
De reeks ip-adressen  ip-adres   die behoren tot 192.168.0.0/25 begint met 192.168.0.1 en eindigt met 192.168.0.126, de network id is 192.168.0.0 en het `broadcast`  broadcast   adres is 192.168.0.127. 
Als we dit binair voorstellen wordt het duidelijk: 
<table frame='all'>## /25 binair bekijken 
<?dbfo table-width="99%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="4*" align="center"/>
<colspec colname='c2' colwidth="11*" align="center"/>
<colspec colname='c3' colwidth="5*" align="center"/>
<thead>
<row>
<entry>omschrijving</entry>
<entry>binair</entry>
<entry>decimaal</entry>
</row>
</thead>
<tbody>
<row>
<entry>network id</entry>
<entry>11000000.10101000.00000000.00000000</entry>
<entry>192.168.0.0</entry>
</row>
<row>
<entry>subnet mask</entry>
<entry>11111111.11111111.11111111.10000000</entry>
<entry>255.255.255.128</entry>
</row>
<row>
<entry>eerste ip</entry>
<entry>11000000.10101000.00000000.00000001</entry>
<entry>192.168.0.1</entry>
</row>
<row>
<entry>tweede ip</entry>
<entry>11000000.10101000.00000000.00000010</entry>
<entry>192.168.0.2</entry>
</row>
<row>
<entry>derde ip</entry>
<entry>11000000.10101000.00000000.00000011</entry>
<entry>192.168.0.3</entry>
</row>
<row>
<entry>vierde ip</entry>
<entry>11000000.10101000.00000000.00000100</entry>
<entry>192.168.0.4</entry>
</row>
<row>
<entry>vijfde ip</entry>
<entry>11000000.10101000.00000000.00000101</entry>
<entry>192.168.0.5</entry>
</row>
<row>
<entry>...</entry>
<entry>...</entry>
<entry>...</entry>
</row>
<row>
<entry>voorlaatste ip</entry>
<entry>11000000.10101000.00000000.01111101</entry>
<entry>192.168.0.125</entry>
</row>
<row>
<entry>laatste ip</entry>
<entry>11000000.10101000.00000000.01111110</entry>
<entry>192.168.0.126</entry>
</row>
<row>
<entry>broadcast ip</entry>
<entry>11000000.10101000.00000000.01111111</entry>
<entry>192.168.0.127</entry>
</row>
</tbody>
</tgroup>
</table>
We voegen nog een `bit`  bit   toe aan de `subnet mask`  subnet mask  , dan komen we aan /26 (255.255.255.192). De tabel ziet er dan als volgt uit: 
<table frame='all'>## /26 binair  binair   bekijken 
<?dbfo table-width="99%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="4*" align="center"/>
<colspec colname='c2' colwidth="11*" align="center"/>
<colspec colname='c3' colwidth="5*" align="center"/>
<thead>
<row>
<entry>omschrijving</entry>
<entry>binair</entry>
<entry>decimaal</entry>
</row>
</thead>
<tbody>
<row>
<entry>network id</entry>
<entry>11000000.10101000.00000000.00000000</entry>
<entry>192.168.0.0</entry>
</row>
<row>
<entry>subnet mask</entry>
<entry>11111111.11111111.11111111.11000000</entry>
<entry>255.255.255.192</entry>
</row>
<row>
<entry>eerste ip</entry>
<entry>11000000.10101000.00000000.00000001</entry>
<entry>192.168.0.1</entry>
</row>
<row>
<entry>tweede ip</entry>
<entry>11000000.10101000.00000000.00000010</entry>
<entry>192.168.0.2</entry>
</row>
<row>
<entry>derde ip</entry>
<entry>11000000.10101000.00000000.00000011</entry>
<entry>192.168.0.3</entry>
</row>
<row>
<entry>...</entry>
<entry>...</entry>
<entry>...</entry>
</row>
<row>
<entry>voorlaatste ip</entry>
<entry>11000000.10101000.00000000.00111101</entry>
<entry>192.168.0.61</entry>
</row>
<row>
<entry>laatste ip</entry>
<entry>11000000.10101000.00000000.00111110</entry>
<entry>192.168.0.62</entry>
</row>
<row>
<entry>broadcast ip</entry>
<entry>11000000.10101000.00000000.00111111</entry>
<entry>192.168.0.63</entry>
</row>
</tbody>
</tgroup>
</table>


---

# binaire subnets decimaal voorstellen 
We weten al dat 255 `decimaal`  decimaal   gelijk is aan 11111111 `binair`  binair   en dat 0 decimaal gelijk is aan 00000000 binair (in byte-vorm). Een binair `subnet mask`  subnet mask   begint steeds met eentjes, en eindigt met nulletjes. De volgende tabel kan dus handig zijn bij het decimaal neerschrijven van een binair subnet mask. 
<table frame='all'>## decimale waarde binaire subnet bytes 
<?dbfo table-width="70%" ?>
<tgroup cols='2' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="1*" align="center"/>
<thead>
<row>
<entry>binair subnet mask</entry>
<entry>decimaal getal</entry>
</row>
</thead>
<tbody>
<row>
<entry>11111111</entry>
<entry>255</entry>
</row>
<row>
<entry>11111110</entry>
<entry>254</entry>
</row>
<row>
<entry>11111100</entry>
<entry>252</entry>
</row>
<row>
<entry>11111000</entry>
<entry>248</entry>
</row>
<row>
<entry>11110000</entry>
<entry>240</entry>
</row>
<row>
<entry>11100000</entry>
<entry>224</entry>
</row>
<row>
<entry>11000000</entry>
<entry>192</entry>
</row>
<row>
<entry>10000000</entry>
<entry>128</entry>
</row>
<row>
<entry>00000000</entry>
<entry>0</entry>
</row>
</tbody>
</tgroup>
</table>


---

# 32 binaire subnet masks 
Bij supernetting zijn er theoretisch 32 `binaire`  binair   `subnet masks`  subnet mask   i.p.v. de drie `classful`  classful   (255.255.255.0, 255.255.0.0, 255.0.0.0). We zetten er 31 op een rijtje. 
<table frame='all'>## 31 binaire subnets 
<?dbfo table-width="70%" ?>
<tgroup cols='2' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="2*" align="center"/>
<thead>
<row>
<entry>aantal bits op 1</entry>
<entry>decimale waarde</entry>
</row>
</thead>
<tbody>
<row>
<entry>1</entry>
<entry>128.0.0.0</entry>
</row>
<row>
<entry>2</entry>
<entry>192.0.0.0</entry>
</row>
<row>
<entry>3</entry>
<entry>224.0.0.0</entry>
</row>
<row>
<entry>4</entry>
<entry>240.0.0.0</entry>
</row>
<row>
<entry>5</entry>
<entry>248.0.0.0</entry>
</row>
<row>
<entry>6</entry>
<entry>252.0.0.0</entry>
</row>
<row>
<entry>7</entry>
<entry>254.0.0.0</entry>
</row>
<row>
<entry>8</entry>
<entry>255.0.0.0</entry>
</row>
<row>
<entry>9</entry>
<entry>255.128.0.0</entry>
</row>
<row>
<entry>10</entry>
<entry>255.192.0.0</entry>
</row>
<row>
<entry>11</entry>
<entry>255.224.0.0</entry>
</row>
<row>
<entry>12</entry>
<entry>255.240.0.0</entry>
</row>
<row>
<entry>13</entry>
<entry>255.248.0.0</entry>
</row>
<row>
<entry>14</entry>
<entry>255.252.0.0</entry>
</row>
<row>
<entry>15</entry>
<entry>255.254.0.0</entry>
</row>
<row>
<entry>16</entry>
<entry>255.255.0.0</entry>
</row>
<row>
<entry>17</entry>
<entry>255.255.128.0</entry>
</row>
<row>
<entry>18</entry>
<entry>255.255.192.0</entry>
</row>
<row>
<entry>19</entry>
<entry>255.255.224.0</entry>
</row>
<row>
<entry>20</entry>
<entry>255.255.240.0</entry>
</row>
<row>
<entry>21</entry>
<entry>255.255.248.0</entry>
</row>
<row>
<entry>22</entry>
<entry>255.255.252.0</entry>
</row>
<row>
<entry>23</entry>
<entry>255.255.254.0</entry>
</row>
<row>
<entry>24</entry>
<entry>255.255.255.0</entry>
</row>
<row>
<entry>25</entry>
<entry>255.255.255.128</entry>
</row>
<row>
<entry>26</entry>
<entry>255.255.255.192</entry>
</row>
<row>
<entry>27</entry>
<entry>255.255.255.224</entry>
</row>
<row>
<entry>28</entry>
<entry>255.255.255.240</entry>
</row>
<row>
<entry>29</entry>
<entry>255.255.255.248</entry>
</row>
<row>
<entry>30</entry>
<entry>255.255.255.252</entry>
</row>
<row>
<entry>31</entry>
<entry>255.255.255.254</entry>
</row>
</tbody>
</tgroup>
</table>


# aantal computers 
We kunnen de tabel uitbreiden met een kolom die het aantal computers telt dat we in deze `binaire`  binair   netwerkjes kunnen plaatsen. 
De simpelste formule om het aantal computers in een `subnet`  subnet   te berekenen is twee verheffen tot de macht 'het aantal bits op 0 in de subnet' min twee. Bij 16 bits op 1 wordt dat dus 2 tot de zestiende min twee. In de tabel gebruiken we 256 i.p.v. twee tot de achtste. 
<table frame='all'>## aantal computers in binaire subnets 
<?dbfo table-width="90%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="1*" align="center"/>
<colspec colname='c3' colwidth="1*" align="center"/>
<thead>
<row>
<entry>aantal bits op 1</entry>
<entry>berekening</entry>
<entry>aantal computers</entry>
</row>
</thead>
<tbody>
<row>
<entry>8</entry>
<entry>256*256*256-2</entry>
<entry>16777214</entry>
</row>
<row>
<entry>9</entry>
<entry>256*256*128-2</entry>
<entry>8388606</entry>
</row>
<row>
<entry>10</entry>
<entry>256*256*64-2</entry>
<entry>4194302</entry>
</row>
<row>
<entry>11</entry>
<entry>256*256*32-2</entry>
<entry>2097150</entry>
</row>
<row>
<entry>12</entry>
<entry>256*256*16-2</entry>
<entry>1048574</entry>
</row>
<row>
<entry>13</entry>
<entry>256*256*8-2</entry>
<entry>524286</entry>
</row>
<row>
<entry>14</entry>
<entry>256*256*4-2</entry>
<entry>262142</entry>
</row>
<row>
<entry>15</entry>
<entry>256*256*2-2</entry>
<entry>131070</entry>
</row>
<row>
<entry>16</entry>
<entry>256*256-2</entry>
<entry>65534</entry>
</row>
<row>
<entry>17</entry>
<entry>256*128-2</entry>
<entry>32766</entry>
</row>
<row>
<entry>18</entry>
<entry>256*64-2</entry>
<entry>16382</entry>
</row>
<row>
<entry>19</entry>
<entry>256*32-2</entry>
<entry>8190</entry>
</row>
<row>
<entry>20</entry>
<entry>256*16-2</entry>
<entry>4094</entry>
</row>
<row>
<entry>21</entry>
<entry>256*8-2</entry>
<entry>2046</entry>
</row>
<row>
<entry>22</entry>
<entry>256*4-2</entry>
<entry>1022</entry>
</row>
<row>
<entry>23</entry>
<entry>256*2-2</entry>
<entry>510</entry>
</row>
<row>
<entry>24</entry>
<entry>256-2</entry>
<entry>254</entry>
</row>
<row>
<entry>25</entry>
<entry>128-2</entry>
<entry>126</entry>
</row>
<row>
<entry>26</entry>
<entry>64-2</entry>
<entry>62</entry>
</row>
<row>
<entry>27</entry>
<entry>32-2</entry>
<entry>30</entry>
</row>
<row>
<entry>28</entry>
<entry>16-2</entry>
<entry>14</entry>
</row>
<row>
<entry>29</entry>
<entry>8-2</entry>
<entry>6</entry>
</row>
<row>
<entry>30</entry>
<entry>4-2</entry>
<entry>2</entry>
</row>
</tbody>
</tgroup>
</table>
Zie ook eens op `http://www.rfc-editor.org/rfc/rfc1878.txt`. 


---

# network id en host id vinden 
Als we een `ip-adres`  ip-adres   krijgen, kunnen we dan het `network id`  network id   en het `host id`  host id   vinden ? 
We zullen beginnen met een simpel classful voorbeeld: `192.168.1.5/24`. 
```sh

ip-adres    : 192.168.  1.5
subnet mask : `255.255.255`.0
network id  : `192.168.  1`.0
host id     :             `5`
```
We kunnen dit ook binair bekijken: 
```sh

ip-adres    : 11000000.10101000.00000001.00000101
subnet mask : `11111111.11111111.11111111`.00000000
network id  : `11000000.10101000.00000001`.00000000
host id     : 00000000.00000000.00000000.`00000101`
```

Nog een tweede voorbeeld: `192.168.199.233/24`. 
```sh

ip-adres    : 192.168.199.233
subnet mask : `255.255.255`.0
network id  : `192.168.199`.0
host id     :             `233`
```
We kunnen dit tweede voorbeeld ook binair bekijken. 
```sh

ip-adres    : 11000000.10101000.11000111.11101001
subnet mask : `11111111.11111111.11111111`.00000000
network id  : `11000000.10101000.11000111`.00000000
host id     : 00000000.00000000.00000000.`11101001`
```

Als derde voorbeeld zullen we hetzelfde ip-adres nemen als in het tweede, maar met een supernet: `192.168.199.233/22`. We bekijken het eerst binair, want dit is het eenvoudigste. 
```sh

ip-adres    : 11000000.10101000.11000111.11101001
subnet mask : `11111111.11111111.111111`00.00000000
network id  : `11000000.10101000.110001`00.00000000
host id     : 00000000.00000000.000000`11.11101001`
```
Decimaal wordt dat dan: 
```sh

ip-adres    : 192.168.199.233
subnet mask : `255.255.252`.0
network id  : `192.168.196`.0
host id     :           `3.233`
```





---

# voorbeeldoefening binaire subnets 
De vraag "Zitten de volgende computers in hetzelfde netwerk?" was met classful subnets niet zo moeilijk. Deze vraag komt bijna letterlijk terug op het examen, maar dan met binaire subnet masks. 
Vul de volgende tabel aan voor `192.168.234.234/17`. 
<table frame='all'>## oefening 192.168.234.234/17 
<?dbfo table-width="99%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="left"/>
<colspec colname='c2' colwidth="7*" align="center"/>
<colspec colname='c3' colwidth="3*" align="center"/>
<thead>
<row>
<entry></entry>
<entry>binair</entry>
<entry>decimaal</entry>
</row>
</thead>
<tbody>
<row>
<entry>ip address</entry>
<entry>11000000.10101000.11101010.11101010</entry>
<entry>192.168.234.234</entry>
</row>
<row>
<entry>subnet mask</entry>
<entry>11111111.11111111.10000000.00000000</entry>
<entry>255.255.128.0</entry>
</row>
<row>
<entry>network id</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>eerste ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>laatste ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>broadcast ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>aantal ip's</entry>
<entry></entry>
<entry></entry>
</row>
</tbody>
</tgroup>
</table>
Hieronder vind je de oplossing van bovenstaande oefening. 
<table frame='all'>## oplossing 192.168.234.234/17 
<?dbfo table-width="99%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="left"/>
<colspec colname='c2' colwidth="7*" align="center"/>
<colspec colname='c3' colwidth="3*" align="center"/>
<thead>
<row>
<entry></entry>
<entry>binair</entry>
<entry>decimaal</entry>
</row>
</thead>
<tbody>
<row>
<entry>ip address</entry>
<entry>11000000.10101000.11101010.11101010</entry>
<entry>192.168.234.234</entry>
</row>
<row>
<entry>subnet mask</entry>
<entry>11111111.11111111.10000000.00000000</entry>
<entry>255.255.128.0</entry>
</row>
<row>
<entry>network id</entry>
<entry>11000000.10101000.10000000.00000000</entry>
<entry>192.168.128.0</entry>
</row>
<row>
<entry>eerste ip</entry>
<entry>11000000.10101000.10000000.00000001</entry>
<entry>192.168.128.1</entry>
</row>
<row>
<entry>laatste ip</entry>
<entry>11000000.10101000.11111111.11111110</entry>
<entry>192.168.255.254</entry>
</row>
<row>
<entry>broadcast ip</entry>
<entry>11000000.10101000.11111111.11111111</entry>
<entry>192.168.255.255</entry>
</row>
<row>
<entry>aantal ip's</entry>
<entry>van 0000000.00000001 tot 1111111.11111110</entry>
<entry>128*256-2=32766</entry>
</row>
</tbody>
</tgroup>
</table>
---

Het bovenstaande netwerk bevat exact de helft van alle ip-adressen in de 192.168.0.0/16 reeks. Kan je de tabel ook invullen voor de andere helft ? 
<table frame='all'>## andere helft van 192.168.234.234/17 
<?dbfo table-width="99%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="left"/>
<colspec colname='c2' colwidth="7*" align="center"/>
<colspec colname='c3' colwidth="3*" align="center"/>
<thead>
<row>
<entry></entry>
<entry>binair</entry>
<entry>decimaal</entry>
</row>
</thead>
<tbody>
<row>
<entry>ip address</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>subnet mask</entry>
<entry>11111111.11111111.10000000.00000000</entry>
<entry>255.255.128.0</entry>
</row>
<row>
<entry>network id</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>eerste ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>laatste ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>broadcast ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>aantal ip's</entry>
<entry></entry>
<entry></entry>
</row>
</tbody>
</tgroup>
</table>
Hieronder de oplossing van de andere helft. 
<table frame='all'>## oplossing andere helft 192.168.234.234/17 
<?dbfo table-width="99%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="left"/>
<colspec colname='c2' colwidth="7*" align="center"/>
<colspec colname='c3' colwidth="3*" align="center"/>
<thead>
<row>
<entry></entry>
<entry>binair</entry>
<entry>decimaal</entry>
</row>
</thead>
<tbody>
<row>
<entry>ip address</entry>
<entry>11000000.10101000.`0`1101010.11101010</entry>
<entry>192.168.106.234</entry>
</row>
<row>
<entry>subnet mask</entry>
<entry>11111111.11111111.10000000.00000000</entry>
<entry>255.255.128.0</entry>
</row>
<row>
<entry>network id</entry>
<entry>11000000.10101000.`0`0000000.00000000</entry>
<entry>192.168.0.0</entry>
</row>
<row>
<entry>eerste ip</entry>
<entry>11000000.10101000.`0`0000000.00000001</entry>
<entry>192.168.0.1</entry>
</row>
<row>
<entry>laatste ip</entry>
<entry>11000000.10101000.`0`1111111.11111110</entry>
<entry>192.168.127.254</entry>
</row>
<row>
<entry>broadcast ip</entry>
<entry>11000000.10101000.`0`1111111.11111111</entry>
<entry>192.168.127.255</entry>
</row>
<row>
<entry>aantal ip's</entry>
<entry>van 0000000.00000001 tot 1111111.11111110</entry>
<entry>128*256-2=32766</entry>
</row>
</tbody>
</tgroup>
</table>


---

# oefeningen binaire subnets 
Probeer nu dezelfde oefening voor: 
```sh

168.186.240.192/11
192.168.248.234/17
168.190.248.199/27
```
Kan je de network id, subnet mask, eerste ip, laatste ip, broadcast ip en aantal ip's geven voor de subnets van die drie ip-adressen ? Zonder naar de oplossing hieronder te kijken ? 
Hieronder eerst drie lege tabellen om te oefenen, dan de oplossing. 
<table frame='all'>## lege tabel 168.186.240.192/11 
<?dbfo table-width="99%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="left"/>
<colspec colname='c2' colwidth="7*" align="center"/>
<colspec colname='c3' colwidth="3*" align="center"/>
<thead>
<row>
<entry></entry>
<entry>binair</entry>
<entry>decimaal</entry>
</row>
</thead>
<tbody>
<row>
<entry>ip address</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>subnet mask</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>network id</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>eerste ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>laatste ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>broadcast ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>aantal ip's</entry>
<entry></entry>
<entry></entry>
</row>
</tbody>
</tgroup>
</table>

<table frame='all'>## lege tabel 192.168.248.234/17 
<?dbfo table-width="99%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="left"/>
<colspec colname='c2' colwidth="7*" align="center"/>
<colspec colname='c3' colwidth="3*" align="center"/>
<thead>
<row>
<entry></entry>
<entry>binair</entry>
<entry>decimaal</entry>
</row>
</thead>
<tbody>
<row>
<entry>ip address</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>subnet mask</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>network id</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>eerste ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>laatste ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>broadcast ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>aantal ip's</entry>
<entry></entry>
<entry></entry>
</row>
</tbody>
</tgroup>
</table>

<table frame='all'>## lege tabel 168.190.248.199/27 
<?dbfo table-width="99%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="left"/>
<colspec colname='c2' colwidth="7*" align="center"/>
<colspec colname='c3' colwidth="3*" align="center"/>
<thead>
<row>
<entry></entry>
<entry>binair</entry>
<entry>decimaal</entry>
</row>
</thead>
<tbody>
<row>
<entry>ip address</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>subnet mask</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>network id</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>eerste ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>laatste ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>broadcast ip</entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>aantal ip's</entry>
<entry></entry>
<entry></entry>
</row>
</tbody>
</tgroup>
</table>
---

<table frame='all'>## oplossing 168.186.240.192/11 
<?dbfo table-width="99%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="left"/>
<colspec colname='c2' colwidth="7*" align="center"/>
<colspec colname='c3' colwidth="3*" align="center"/>
<thead>
<row>
<entry></entry>
<entry>binair</entry>
<entry>decimaal</entry>
</row>
</thead>
<tbody>
<row>
<entry>ip address</entry>
<entry>10101000.10111010.11110000.11000000</entry>
<entry>168.186.240.192</entry>
</row>
<row>
<entry>subnet mask</entry>
<entry>`11111111.111`00000.00000000.00000000</entry>
<entry>255.224.0.0</entry>
</row>
<row>
<entry>network id</entry>
<entry>`10101000.101`00000.00000000.00000000</entry>
<entry>168.160.0.0</entry>
</row>
<row>
<entry>eerste ip</entry>
<entry>`10101000.101`00000.00000000.0000000`1`</entry>
<entry>168.160.0.1</entry>
</row>
<row>
<entry>laatste ip</entry>
<entry>`10101000.101`11111.11111111.11111110</entry>
<entry>168.191.255.254</entry>
</row>
<row>
<entry>broadcast ip</entry>
<entry>`10101000.101`11111.11111111.11111111</entry>
<entry>168.191.255.255</entry>
</row>
<row>
<entry>aantal ip's</entry>
<entry>van 00000.00000000.00000001 tot 11111.11111111.11111110</entry>
<entry>32*256*256-2</entry>
</row>
</tbody>
</tgroup>
</table>

<table frame='all'>## oplossing 192.168.248.234/17 
<?dbfo table-width="99%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="left"/>
<colspec colname='c2' colwidth="7*" align="center"/>
<colspec colname='c3' colwidth="3*" align="center"/>
<thead>
<row>
<entry></entry>
<entry>binair</entry>
<entry>decimaal</entry>
</row>
</thead>
<tbody>
<row>
<entry>ip address</entry>
<entry>11000000.10101000.11111000.11101010</entry>
<entry>192.168.248.234</entry>
</row>
<row>
<entry>subnet mask</entry>
<entry>`11111111.11111111.1`0000000.00000000</entry>
<entry>255.255.128.0</entry>
</row>
<row>
<entry>network id</entry>
<entry>`11000000.10101000.1`0000000.00000000</entry>
<entry>192.168.128.0</entry>
</row>
<row>
<entry>eerste ip</entry>
<entry>`11000000.10101000.1`0000000.0000000`1`</entry>
<entry>192.168.128.1</entry>
</row>
<row>
<entry>laatste ip</entry>
<entry>`11000000.10101000.1`1111111.11111110</entry>
<entry>192.168.255.254</entry>
</row>
<row>
<entry>broadcast ip</entry>
<entry>`11000000.10101000.1`1111111.11111111</entry>
<entry>192.168.255.255</entry>
</row>
<row>
<entry>aantal ip's</entry>
<entry>van 0000000.00000001 tot 1111111.11111110</entry>
<entry>128*256-2</entry>
</row>
</tbody>
</tgroup>
</table>

<table frame='all'>## oplossing 168.190.248.199/27 
<?dbfo table-width="99%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="left"/>
<colspec colname='c2' colwidth="7*" align="center"/>
<colspec colname='c3' colwidth="3*" align="center"/>
<thead>
<row>
<entry></entry>
<entry>binair</entry>
<entry>decimaal</entry>
</row>
</thead>
<tbody>
<row>
<entry>ip address</entry>
<entry>10101000.10111110.11111000.11000111</entry>
<entry>168.190.248.199</entry>
</row>
<row>
<entry>subnet mask</entry>
<entry>`11111111.11111111.11111111.111`00000</entry>
<entry>255.255.255.224</entry>
</row>
<row>
<entry>network id</entry>
<entry>`10101000.10111110.11111000.110`00000</entry>
<entry>168.190.248.192</entry>
</row>
<row>
<entry>eerste ip</entry>
<entry>`10101000.10111110.11111000.110`0000`1`</entry>
<entry>168.190.248.193</entry>
</row>
<row>
<entry>laatste ip</entry>
<entry>`10101000.10111110.11111000.110`11110</entry>
<entry>168.190.248.222</entry>
</row>
<row>
<entry>broadcast ip</entry>
<entry>`10101000.10111110.11111000.110`11111</entry>
<entry>168.190.248.223</entry>
</row>
<row>
<entry>aantal ip's</entry>
<entry>van 00001 tot 11110</entry>
<entry>32-2</entry>
</row>
</tbody>
</tgroup>
</table>


# zelfde of ander netwerk ? 
Zitten de computers 192.168.117.5/18 en 192.168.34.18/18 in hetzelfde netwerk ? 




---

# subnetworks 
Je beheert een departement met 200 computers, verdeeld over vier verdiepingen in eenzelfde gebouw, met ongeveer vijftig computers per verdieping. Je krijgt van je netwerkbeheerder `192.168.5.0/24` en moet deze reeks verdelen over de vier verdiepen. Hoe doe je dat ? 
Je begint met uit te rekenen hoeveel computers er in die reeks kunnen: 
```sh

192.168.5.0/24 --> 256 - 2 = 254 computers
```
Je weet ook uit de tabel hierboven dat een `/26` `subnet mask`  subnet mask   volstaat voor elke individuele verdieping. Je verdeelt jouw /24 netwerk in vier /26 netwerken. 
We bekijken dit eerst binair: 
```sh

192.168.5.0 == 11000000.10101000.00000101.00000000
/24 mask    == 11111111.11111111.11111111.00000000
/26 mask    == 11111111.11111111.11111111.11000000
```
We moeten dus twee `bits`  bit   toevoegen aan de /24 `netword id`  network id   om een /26 network id te maken. Twee bits kunnen exact vier mogelijke waarden hebben: 00, 01, 10 of 11. We komen dus tot de volgende vier nieuwe /26 network id's. 
```sh

/26 network id 1 == 11000000.10101000.00000101.`00`000000
/26 network id 2 == 11000000.10101000.00000101.`01`000000
/26 network id 3 == 11000000.10101000.00000101.`10`000000
/26 network id 4 == 11000000.10101000.00000101.`11`000000
```
Hieronder een tabel van eerste en laatste ip van de vier /26 netwerken. Tel bij de laatste ip eentje bij voor de broadcast ip. 
<table frame='all'>## echt supernetten 
<?dbfo table-width="80%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="center"/>
<colspec colname='c2' colwidth="2*" align="center"/>
<colspec colname='c3' colwidth="2*" align="center"/>
<thead>
<row>
<entry>verdieping</entry>
<entry>eerste ip</entry>
<entry>laatste ip</entry>
</row>
</thead>
<tbody>
<row>
<entry>1</entry>
<entry>192.168.5.1</entry>
<entry>192.168.5.62</entry>
</row>
<row>
<entry>2</entry>
<entry>192.168.5.65</entry>
<entry>192.168.5.126</entry>
</row>
<row>
<entry>3</entry>
<entry>192.168.5.129</entry>
<entry>192.168.5.190</entry>
</row>
<row>
<entry>4</entry>
<entry>192.168.5.193</entry>
<entry>192.168.5.254</entry>
</row>
</tbody>
</tgroup>
</table>
Tussen haakjes, nu zijn we echt aan het `supernetten`  supernet  . Want in de routers van buitenaf staat er enkel 192.168.5.0/24 als bestemming, terwijl er intern eigenlijk vier /26 netwerkjes zijn. 



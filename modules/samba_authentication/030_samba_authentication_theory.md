---

# creating the users on Linux 
The goal of this example is to set up a file share accessible to a number of different users. The users will need to authenticate with their password before access to this share is granted. We will first create three randomly named users, each with their own password. First we add these users to Linux. 
```sh

[root@RHEL52 ~]# useradd -c "Serena Williams" serena
[root@RHEL52 ~]# useradd -c "Justine Henin" justine
[root@RHEL52 ~]# useradd -c "Martina Hingis" martina
[root@RHEL52 ~]# passwd serena
Changing password for user serena.
New UNIX password: 
Retype new UNIX password: 
passwd: all authentication tokens updated successfully.
[root@RHEL52 ~]# passwd justine
Changing password for user justine.
New UNIX password: 
Retype new UNIX password: 
passwd: all authentication tokens updated successfully.
[root@RHEL52 ~]# passwd martina
Changing password for user martina.
New UNIX password: 
Retype new UNIX password: 
passwd: all authentication tokens updated successfully.
```


# creating the users on samba 
Then we add them to the `smbpasswd`  smbpasswd(8)   file, with the same password. 
```sh

[root@RHEL52 ~]# smbpasswd -a serena
New SMB password:
Retype new SMB password:
Added user serena.
[root@RHEL52 ~]# smbpasswd -a justine
New SMB password:
Retype new SMB password:
Added user justine.
[root@RHEL52 ~]# smbpasswd -a martina
New SMB password:
Retype new SMB password:
Added user martina.
```


# security = user 
Remember that we set samba's security mode to share with the `security = share`  security mode(samba)   directive in the [global] section ? Since we now require users to always provide a userid and password for access to our samba server, we will need to change this. Setting `security = user` will require the client to provide samba with a valid userid and password before giving access to a share. 
Our [global] section now looks like this. 
```sh

[global]
workgroup = WORKGROUP
netbios name = TEACHER0
server string = Samba File Server
security = user
```


# configuring the share 
We add the following [share] section to our smb.conf (and we do not forget to create the directory /srv/samba/authwrite). 
```sh

[authwrite]
path = /srv/samba/authwrite
comment = authenticated users only
read only = no
guest ok = no
```


# testing access with net use 
After restarting samba, we test with different users from within Microsoft computers. The screenshots use the `net use`  net use(microsoft)  First serena from Windows XP. 
```sh

C:\&#062;net use m: \\teacher0\authwrite stargate /user:serena
The command completed successfully.

C:\&#062;m:

M:\&#062;echo greetings from Serena &#062; serena.txt
```
The next screenshot is martina on a Windows 2000 computer, she succeeds in writing her files, but fails to overwrite the file from serena. 
```sh

C:\&#062;net use k: \\teacher0\authwrite stargate /user:martina
The command completed successfully.

C:\&#062;k:

K:\&#062;echo greetings from martina &#062; Martina.txt

K:\&#062;echo test overwrite &#062; serena.txt
Access is denied.
```


# testing access with smbclient 
You can also test connecting with authentication with `smbclient`  smbclient(1)  . First we test with a wrong password. 
```sh

[root@RHEL52 samba]# smbclient //teacher0/authwrite -U martina wrongpass
session setup failed: NT_STATUS_LOGON_FAILURE
```
Then we test with the correct password, and verify that we can access a file on the share. 
```sh

[root@RHEL52 samba]# smbclient //teacher0/authwrite -U martina stargate
Domain=[TEACHER0] OS=[Unix] Server=[Samba 3.0.33-3.7.el5]
smb: \> more serena.txt 
getting file \serena.txt of size 14 as /tmp/smbmore.QQfmSN (6.8 kb/s)
one
two
three
smb: \> q
```


# verify ownership 
We now have a simple standalone samba file server with authenticated access. And the files in the shares belong to their proper owners. 
```sh

[root@RHEL52 samba]# ls -l /srv/samba/authwrite/
total 8
-rwxr--r-- 1 martina martina  0 Jan 21 20:06 martina.txt
-rwxr--r-- 1 serena  serena  14 Jan 21 20:06 serena.txt
-rwxr--r-- 1 serena  serena   6 Jan 21 20:09 ser.txt
```


# common problems 
# NT_STATUS_BAD_NETWORK_NAME 
You can get `NT_STATUS_BAD_NETWORK_NAME`  NT_STATUS_BAD_NETWORK_NAME   when you forget to create the target directory. 
```sh

[root@RHEL52 samba]# rm -rf /srv/samba/authwrite/
[root@RHEL52 samba]# smbclient //teacher0/authwrite -U martina stargate
Domain=[TEACHER0] OS=[Unix] Server=[Samba 3.0.33-3.7.el5]
tree connect failed: NT_STATUS_BAD_NETWORK_NAME
```


# NT_STATUS_LOGON_FAILURE 
You can get `NT_STATUS_LOGON_FAILURE`  NT_STATUS_LOGON_FAILURE   when you type the wrong password or when you type an unexisting username. 
```sh

[root@RHEL52 samba]# smbclient //teacher0/authwrite -U martina STARGATE
session setup failed: NT_STATUS_LOGON_FAILURE
```


# usernames are (not) case sensitive 
Remember that usernames om Linux are case sensitive. 
```sh

[root@RHEL52 samba]# su - MARTINA
su: user MARTINA does not exist
[root@RHEL52 samba]# su - martina
[martina@RHEL52 ~]$ 
```
But usernames on Microsoft computers are not case sensitive. 
```sh

[root@RHEL52 samba]# smbclient //teacher0/authwrite -U martina stargate
Domain=[TEACHER0] OS=[Unix] Server=[Samba 3.0.33-3.7.el5]
smb: \> q
[root@RHEL52 samba]# smbclient //teacher0/authwrite -U MARTINA stargate
Domain=[TEACHER0] OS=[Unix] Server=[Samba 3.0.33-3.7.el5]
smb: \> q
```





---

# Practice: introduction to users 
1. Run a command that displays only your currently logged on user name. 
2. Display a list of all logged on users. 
3. Display a list of all logged on users including the command they are running at this very moment. 
4. Display your user name and your unique user identification (userid). 
5. Use `su` to switch to another user account (unless you are root, you will need the password of the other account). And get back to the previous account. 
6. Now use `su -` to switch to another user and notice the difference. 
Note that `su -` gets you into the home directory of, `Tania`. 
7. Try create a new user account with `sudo` before `useradd` command. 
8. Try to create a new user account without using `sudo` command. this should fail. (Details on adding user accounts are explained in the next chapter.) 




---

# Practice: introduction to scripting 
0. Give each script a different name, keep them for later! 
1. Write a script that outputs the name of a city. 
2. Make sure the script runs in the bash shell. 
3. Make sure the script runs in the Korn shell. 
4. Create a script that defines two variables, and outputs their value. 
5. The previous script does not influence your current shell (the variables do not exist outside of the script). Now run the script so that it influences your current shell. 
6. Is there a shorter way to `source` the script ? 
7. Comment your scripts so that others may know what they are working with. 



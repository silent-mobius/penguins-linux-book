---

# solution : process priorities 
1. Create a new directory and create six `pipes` in that directory. 
```sh
[paul@rhel53 ~]$ mkdir pipes ; cd pipes
[paul@rhel53 pipes]$ mkfifo p1 p2 p3 p4 p5 p6
[paul@rhel53 pipes]$ ls -l
total 0
prw-rw-r-- 1 paul paul 0 Apr 12 22:15 p1
prw-rw-r-- 1 paul paul 0 Apr 12 22:15 p2
prw-rw-r-- 1 paul paul 0 Apr 12 22:15 p3
prw-rw-r-- 1 paul paul 0 Apr 12 22:15 p4
prw-rw-r-- 1 paul paul 0 Apr 12 22:15 p5
prw-rw-r-- 1 paul paul 0 Apr 12 22:15 p6```
2. Bounce a character between two `pipes`. 
```sh
[paul@rhel53 pipes]$ echo -n x | cat - p1 &#062; p2 &#038;
[1] 4013
[paul@rhel53 pipes]$ cat &#060;p2 &#062;p1 &#038;
[2] 4016```
3. Use `top` and `ps` to display information (pid, ppid, priority, nice value, ...) about these two cat processes. 
```sh
top (probably the top two lines)

[paul@rhel53 pipes]$ ps -C cat
PID TTY          TIME CMD
4013 pts/0    00:03:38 cat
4016 pts/0    00:01:07 cat

[paul@rhel53 pipes]$ ps fax | grep cat
4013 pts/0    R      4:00  |           \_ cat - p1
4016 pts/0    S      1:13  |           \_ cat
4044 pts/0    S+     0:00  |           \_ grep cat```
4. Bounce another character between two other pipes, but this time start the commands `nice`. Verify that all `cat` processes are battling for the cpu. (Feel free to fire up two more cats with the remaining pipes). 
```sh
echo -n y | nice cat - p3 &#062; p4 &#038;
nice cat &#060;p4 &#062;p3 &#038;```
5. Use `ps` to verify that the two new `cat` processes have a `nice` value. Use the -o and -C options of `ps` for this. 
```sh
[paul@rhel53 pipes]$ ps -C cat -o pid,ppid,pri,ni,comm
PID  PPID PRI  NI COMMAND
4013  3947  14   0 cat
4016  3947  21   0 cat
4025  3947  13  10 cat
4026  3947  13  10 cat```
6. Use `renice` te increase the nice value from 10 to 15. Notice the difference with the usual commands. 
```sh
[paul@rhel53 pipes]$ renice +15 4025
4025: old priority 10, new priority 15
[paul@rhel53 pipes]$ renice +15 4026
4026: old priority 10, new priority 15

[paul@rhel53 pipes]$ ps -C cat -o pid,ppid,pri,ni,comm
PID  PPID PRI  NI COMMAND
4013  3947  14   0 cat
4016  3947  21   0 cat
4025  3947   9  15 cat
4026  3947   8  15 cat
```



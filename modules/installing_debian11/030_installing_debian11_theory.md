---

# Debian 
 Debian is one of the oldest Linux distributions. I use Debian myself on almost every computer that I own (including `raspbian` on the `Raspberry Pi`). 
 Debian comes in `releases` named after characters in the movie `Toy Story`. The `Jessie` release contains about 36000 packages. 
<table frame='all'>## Debian releases 
<?dbfo table-width="50%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="center"/>
<colspec colname='c2' colwidth="2*" align="center"/>
<colspec colname='c3' colwidth="2*" align="center"/>
<thead>
<row>
  <entry>name</entry>
  <entry>number</entry>
  <entry>year</entry>
</row>
</thead>
<tbody>
<row>
  <entry>Woody</entry>
  <entry>3.0</entry>
  <entry>2002</entry>
</row>
<row>
  <entry>Sarge</entry>
  <entry>3.1</entry>
  <entry>2005</entry>
</row>
<row>
  <entry>Etch</entry>
  <entry>4.0</entry>
  <entry>2007</entry>
</row>
<row>
  <entry>Lenny</entry>
  <entry>5.0</entry>
  <entry>2009</entry>
</row>
<row>
  <entry>Squeeze</entry>
  <entry>6.0</entry>
  <entry>2011</entry>
</row>
<row>
  <entry>Wheezy</entry>
  <entry>7</entry>
  <entry>2013</entry>
</row>
<row>
  <entry>Jessie</entry>
  <entry>8</entry>
  <entry>2015</entry>
</row>
<row>
  <entry>Stretch</entry>
  <entry>9</entry>
  <entry>2017</entry>
</row>
<row>
  <entry>Buster</entry>
  <entry>10</entry>
  <entry>2019</entry>
</row>
<row>
  <entry>Bullseye</entry>
  <entry>11</entry>
  <entry>2021</entry>
</row>
</tbody>
</tgroup>
</table>
 There is never a fixed date for the next `Debian` release. The next version is released when it is ready. 


# Downloading 
 All these screenshots were made in November 2014, which means `Debian 8` was still in 'testing' (but in 'freeze', so there will be no major changes when it is released). 
 Download Debian here: 
  <img  src="../img/debian8.png" format="EPS" align="center"> </img>  
 After a couple of clicks on that website, I ended up downloading `Debian 8` (testing) here. It should be only one click once `Debian 8` is released (somewhere in 2015). 
  <img  src="../img/debian10_ftp.png" format="EPS" align="center"> </img>  
 You have many other options to download and install `Debian`. We will discuss them much later. 
 This small screenshot shows the downloading of a `netinst` .iso file. Most of the software will be downloaded during the installation. This also means that you will have the most recent version of all packages when the install is finished. 
  <img  src="../img/debian10_isodown.png" format="EPS" align="center"> </img>  
 I already have Debian 8 installed on my laptop (hence the `paul@debian8` prompt). Anyway, this is the downloaded file just before starting the installation. 
 ```sh
paul@debian8:~$ `ls -hl debian-testing-amd64-netinst.iso`
-rw-r--r-- 1 paul paul 231M Nov 10 17:59 debian-testing-amd64-netinst.iso```
---

 Create a new virtualbox machine (I already have five, you might have zero for now). Click the `New` button to start a wizard that will help you create a virtual machine. 
  <img  src="../img/debian10_vb.png" format="EPS" align="center"> </img>  
 The machine needs a name, this screenshot shows that I named it `server42`. 
  <img  src="../img/debian10_vb2.png" format="EPS" align="center"> </img>  
---

 Most of the defaults in Virtualbox are ok. 
 512MB of RAM is enough to practice all the topics in this book. 
  <img  src="../img/centos8_mem.png" format="EPS" align="center"> </img>  
 We do not care about the virtual disk format. 
  <img  src="../img/centos8_vdi.png" format="EPS" align="center"> </img>  
---

 Choosing `dynamically allocated` will save you some disk space (for a small performance hit). 
  <img  src="../img/centos8_dynamic.png" format="EPS" align="center"> </img>  
 8GB should be plenty for learning about Linux servers. 
  <img  src="../img/debian10_disksize.png" format="EPS" align="center"> </img>  
 This finishes the wizard. You virtual machine is almost ready to begin the installation. 
---

 First, make sure that you attach the downloaded .iso image to the virtual CD drive. (by opening `Settings`, `Storage` followed by a mouse click on the round CD icon) 
  <img  src="../img/debian10_iso.png" format="EPS" align="center"> </img>  
 Personally I also disable sound and usb, because I never use these features. I also remove the floppy disk and use a PS/2 mouse pointer. This is probably not very important, but I like the idea that it saves some resources. 
 Now boot the virtual machine and begin the actual installation. After a couple of seconds you should see a screen similar to this. Choose `Install` to begin the installation of Debian. 
  <img  src="../img/debian10_start_install.png" format="EPS" align="center"> </img>  
---

 First select the language you want to use. 
  <img  src="../img/debian10_inst1.png" format="EPS" align="center"> </img>  
 Choose your country. This information will be used to suggest a download mirror. 
  <img  src="../img/debian10_inst2_country.png" format="EPS" align="center"> </img>  
---

 Choose the correct keyboard. On servers this is of no importance since most servers are remotely managed via `ssh`. 
  <img  src="../img/debian10_inst3_keyb.png" format="EPS" align="center"> </img>  
 Enter a `hostname` (with `fqdn` to set a `dnsdomainname`). 
  <img  src="../img/debian10_inst4_hostname.png" format="EPS" align="center"> </img>  
---

 Give the `root` user a password. Remember this password (or use `hunter2`). 
  <img  src="../img/debian10_inst5_rootpw.png" format="EPS" align="center"> </img>  
 It is adviced to also create a normal user account. I don't give my full name, Debian 8 accepts an identical username and full name `paul`. 
  <img  src="../img/debian10_inst5_newuser.png" format="EPS" align="center"> </img>  
---

 The `use entire disk` refers to the `virtual disk` that you created before in `Virtualbox`.. 
  <img  src="../img/debian10_inst6_disk.png" format="EPS" align="center"> </img>  
 Again the default is probably what you want. Only change partitioning if you really know what you are doing. 
  <img  src="../img/debian10_inst7_partition.png" format="EPS" align="center"> </img>  
---

 Accept the partition layout (again only change if you really know what you are doing). 
  <img  src="../img/debian10_inst8_ponr.png" format="EPS" align="center"> </img>  
 This is the point of no return, the magical moment where pressing `yes` will forever erase data on the (virtual) computer. 
  <img  src="../img/debian10_inst8_ponr2.png" format="EPS" align="center"> </img>  
---

 Software is downloaded from a mirror repository, preferably choose one that is close by (as in the same country). 
  <img  src="../img/debian10_inst9_mirror.png" format="EPS" align="center"> </img>  
 This setup was done in Belgium. 
  <img  src="../img/debian10_inst9_mirror2.png" format="EPS" align="center"> </img>  
---

 Leave the proxy field empty (unless you are sure that you are behind a proxy server). 
  <img  src="../img/debian10_insta_proxy.png" format="EPS" align="center"> </img>  
 Choose whether you want to send anonymous statistics to the Debian project (it gathers data about installed packages). You can view the statistics here `http://popcon.debian.org/`. 
  <img  src="../img/debian10_instb_stats.png" format="EPS" align="center"> </img>  
---

 Choose what software to install, we do not need any graphical stuff for this training. 
  <img  src="../img/debian10_instc_soft.png" format="EPS" align="center"> </img>  
 The latest versions are being downloaded. 
  <img  src="../img/debian10_instd_retr.png" format="EPS" align="center"> </img>  
---

 Say yes to install the bootloader on the virtual machine. 
  <img  src="../img/debian10_inste_grub.png" format="EPS" align="center"> </img>  
 Booting for the first time shows the grub screen 
  <img  src="../img/debian10_grubboot.png" format="EPS" align="center"> </img>  
---

 A couple seconds later you should see a lot of text scrolling of the screen (`dmesg`). After which you are presented with this `getty` and are allowed your first logon. 
  <img  src="../img/debian10_getty.png" format="EPS" align="center"> </img>  
 You should now be able to log on to your virtual machine with the `root` account. Do you remember the password ? Was it `hunter2` ? 
  <img  src="../img/debian10_firstlogon.png" format="EPS" align="center"> </img>  
 The screenshots in this book will look like this from now on. You can just type those commands in the terminal (after you logged on). 
 ```sh
root@server42:~# `who am i`
root     tty1         2014-11-10 18:21
root@server42:~# `hostname`
server42
root@server42:~# `date`
Mon Nov 10 18:21:56 CET 2014```


---

# virtualbox networking 
 You can also log on from remote (or from your Windows/Mac/Linux host computer) using `ssh` or `putty`. Change the `network` settings in the virtual machine to `bridge`. This will enable your virtual machine to receive an ip address from your local dhcp server. 
 The default virtualbox networking is to attach virtual network cards to `nat`. This screenshiot shows the ip address `10.0.2.15` when on `nat`: 
 ```sh
root@server42:~# `ifconfig`
eth0      Link encap:Ethernet  HWaddr 08:00:27:f5:74:cf
          inet addr:10.0.2.15  Bcast:10.0.2.255  Mask:255.255.255.0
          inet6 addr: fe80::a00:27ff:fef5:74cf/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:11 errors:0 dropped:0 overruns:0 frame:0
          TX packets:19 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:2352 (2.2 KiB)  TX bytes:1988 (1.9 KiB)

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)```
  <img  src="../img/debian10_bridge.png" format="EPS" align="center"> </img>  
 By shutting down the network interface and enabling it again, we force Debian to renew an ip address from the bridged network. 
 ```sh
root@server42:~# `# do not run ifdown while connected over ssh!`
root@server42:~# `ifdown eth0`
Killed old client process
Internet Systems Consortium DHCP Client 4.3.1
Copyright 2004-2014 Internet Systems Consortium.
All rights reserved.
For info, please visit https://www.isc.org/software/dhcp/

Listening on LPF/eth0/08:00:27:f5:74:cf
Sending on   LPF/eth0/08:00:27:f5:74:cf
Sending on   Socket/fallback
DHCPRELEASE on eth0 to 10.0.2.2 port 67
root@server42:~# `# now enable bridge in virtualbox settings`
root@server42:~# `ifup eth0`
Internet Systems Consortium DHCP Client 4.3.1
Copyright 2004-2014 Internet Systems Consortium.
All rights reserved.
For info, please visit https://www.isc.org/software/dhcp/

Listening on LPF/eth0/08:00:27:f5:74:cf
Sending on   LPF/eth0/08:00:27:f5:74:cf
Sending on   Socket/fallback
DHCPDISCOVER on eth0 to 255.255.255.255 port 67 interval 8
DHCPDISCOVER on eth0 to 255.255.255.255 port 67 interval 8
DHCPREQUEST on eth0 to 255.255.255.255 port 67
DHCPOFFER from 192.168.1.42
DHCPACK from 192.168.1.42
bound to 192.168.1.111 -- renewal in 2938 seconds.
root@server42:~# `ifconfig eth0`
eth0      Link encap:Ethernet  HWaddr 08:00:27:f5:74:cf
          inet addr:192.168.1.111  Bcast:192.168.1.255  Mask:255.255.255.0
          inet6 addr: fe80::a00:27ff:fef5:74cf/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:15 errors:0 dropped:0 overruns:0 frame:0
          TX packets:31 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:3156 (3.0 KiB)  TX bytes:3722 (3.6 KiB)
root@server42:~#```
 Here is an example of `ssh` to this freshly installed computer. Note that `Debian 8` has disabled remote root access, so i need to use the normal user account. 
 ```sh
paul@debian8:~$ `ssh paul@192.168.1.111`
paul@192.168.1.111's password:

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
paul@server42:~$
paul@server42:~$ `su -`
Password:
root@server42:~#```
 TODO: putty screenshot here... 


---

# setting the hostname 
 The hostname of the server is asked during installation, so there is no need to configure this manually. 
 ```sh
root@server42:~# `hostname`
server42
root@server42:~# `cat /etc/hostname`
server42
root@server42:~# `dnsdomainname`
paul.local
root@server42:~# `grep server42 /etc/hosts`
127.0.1.1       server42.paul.local     server42
root@server42:~#```


# adding a static ip address 
 This example shows how to add a static ip address to your server. 
 You can use `ifconfig` to set a static address that is active until the next `reboot` (or until the next `ifdown`). a
 ```sh
root@server42:~# `ifconfig eth0:0 10.104.33.39````
 Adding a couple of lines to the `/etc/network/interfaces` file to enable an extra ip address forever.  
 ```sh
root@server42:~# `vi /etc/network/interfaces`
root@server42:~# `tail -4 /etc/network/interfaces`
auto eth0:0
iface eth0:0 inet static
address 10.104.33.39
netmask 255.255.0.0
root@server42:~# `ifconfig`
eth0      Link encap:Ethernet  HWaddr 08:00:27:f5:74:cf
          inet addr:192.168.1.111  Bcast:192.168.1.255  Mask:255.255.255.0
          inet6 addr: fe80::a00:27ff:fef5:74cf/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:528 errors:0 dropped:0 overruns:0 frame:0
          TX packets:333 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:45429 (44.3 KiB)  TX bytes:48763 (47.6 KiB)

eth0:0    Link encap:Ethernet  HWaddr 08:00:27:f5:74:cf
          inet addr:10.104.33.39  Bcast:10.255.255.255  Mask:255.0.0.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

root@server42:~#```


---

# Debian package management 
 To get all information about the newest packages form the online repository: 
 ```sh
root@server42:~# `aptitude update`
Get: 1 http://ftp.be.debian.org jessie InRelease [191 kB]
Get: 2 http://security.debian.org jessie/updates InRelease [84.1 kB]
Get: 3 http://ftp.be.debian.org jessie-updates InRelease [117 kB]
Get: 4 http://ftp.be.debian.org jessie-backports InRelease [118 kB]
Get: 5 http://security.debian.org jessie/updates/main Sources [14 B]
Get: 6 http://ftp.be.debian.org jessie/main Sources/DiffIndex [7,876 B]
... (output truncated)```
 To download and apply all updates for all installed packages: 
 ```sh
root@server42:~# `aptitude upgrade`
Resolving dependencies...
The following NEW packages will be installed:
  firmware-linux-free{a} irqbalance{a} libnuma1{a} linux-image-3.16.0-4-amd64{a}
The following packages will be upgraded:
  busybox file libc-bin libc6 libexpat1 libmagic1 libpaper-utils libpaper1 libsqlite3-0
  linux-image-amd64 locales multiarch-support
12 packages upgraded, 4 newly installed, 0 to remove and 0 not upgraded.
Need to get 44.9 MB of archives. After unpacking 161 MB will be used.
Do you want to continue? [Y/n/?]
... (output truncated)```
 To install new software (`vim` and `tmux` in this example): 
 ```sh
root@server42:~# `aptitude install vim tmux`
The following NEW packages will be installed:
  tmux vim vim-runtime{a}
0 packages upgraded, 3 newly installed, 0 to remove and 0 not upgraded.
Need to get 6,243 kB of archives. After unpacking 29.0 MB will be used.
Do you want to continue? [Y/n/?]
Get: 1 http://ftp.be.debian.org/debian/ jessie/main tmux amd64 1.9-6 [245 kB]
Get: 2 http://ftp.be.debian.org/debian/ jessie/main vim-runtime all 2:7.4.488-1 [5,046 kB]
Get: 3 http://ftp.be.debian.org/debian/ jessie/main vim amd64 2:7.4.488-1 [952 kB]```
 Refer to the `package management` chapter in LinuxAdm.pdf for more information. 



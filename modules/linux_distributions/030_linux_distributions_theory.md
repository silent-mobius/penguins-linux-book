---

# Red Hat 
Red Hat is a billion dollar commercial Linux company that puts a lot of effort in developing Linux. They have hundreds of Linux specialists and are known for their excellent support. They give their products (Red Hat Enterprise Linux and Fedora) away for free. While `Red Hat Enterprise Linux`  Red Hat   (RHEL) is well tested before release and supported for up to seven years after release, `Fedora`  Fedora   is a distro with faster updates but without support. 


# Ubuntu 
Canonical started sending out free compact discs with `Ubuntu`  Ubuntu   Linux in 2004 and quickly became popular for home users (many switching from Microsoft Windows). Canonical wants Ubuntu to be an easy to use graphical Linux desktop without need to ever see a command line. Of course they also want to make a profit by selling support for Ubuntu. 


# Debian 
There is no company behind `Debian`  Debian  . Instead there are thousands of well organised developers that elect a `Debian Project Leader` every two years. Debian is seen as one of the most stable Linux distributions. It is also the basis of every release of Ubuntu. Debian comes in three versions: stable, testing and unstable. Every Debian release is named after a character in the movie Toy Story. 


# Other 
Distributions like CentOS  CentOS  , Oracle Enterprise Linux  OEL     Oracle Enterprise Linux  and Scientific Linux  Scientific   are based on Red Hat Enterprise Linux and share many of the same principles, directories and system administration techniques. `Linux Mint`  Linux Mint  , Edubuntu  Edubuntu   and many other *buntu named distributions are based on Ubuntu and thus share a lot with Debian. There are hundreds of other Linux distributions. 


---

# Which to choose ? 
Below are some very personal opinions on some of the most popular Linux Distributions. Keep in mind that any of the below Linux distributions can be a stable server and a nice graphical desktop client. 
<table frame='all'>## choosing a Linux distro 
<?dbfo table-width="99%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="center"/>
<colspec colname='c2' colwidth="5*" align="left"/>
<thead>
<row>
<entry>distribution name</entry>
<entry>reason(s) for using</entry>
</row>
</thead>
<tbody>
<row>
<entry>Red Hat Enterprise (RHEL)</entry>
<entry>You are a manager and you want a good support contract.</entry>
</row>
<row>
<entry>CentOS</entry>
<entry>You want Red Hat without the support contract from Red Hat.</entry>
</row>
<row>
<entry>Fedora</entry>
<entry>You want Red Hat on your laptop/desktop.</entry>
</row>
<row>
<entry>Linux Mint</entry>
<entry>You want a personal graphical desktop to play movies, music and games.</entry>
</row>
<row>
<entry>Debian</entry>
<entry>My personal favorite for servers, laptops, and any other device.</entry>
</row>
<row>
<entry>Ubuntu</entry>
<entry>Very popular, based on Debian, not my favorite.</entry>
</row>
<row>
<entry>Kali</entry>
<entry>You want a pointy-clicky hacking interface.</entry>
</row>
<row>
<entry>others</entry>
<entry>Advanced users may prefer Arch, Gentoo, OpenSUSE, Scientific, ...</entry>
</row>
</tbody>
</tgroup>
</table>
When you are new to Linux in 2015, go for the latest Mint or Fedora. If you only want to practice the Linux command line then install one Debian server and/or one CentOS server (without graphical interface). 
Here are some links to help you choose: 
```sh
distrowatch.com
redhat.com
centos.org
debian.org
www.linuxmint.com
ubuntu.com```



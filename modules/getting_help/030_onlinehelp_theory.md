---

# Google 
Google is a powerful tool to find help about Unix, or anything else. Here are some tricks. 
Look for phrases instead of single words. 
<img  src="img/phrase.jpg" format="EPS" align="center"> </img> 
Search only pages from the .be TLD (or substitute .be for any other Top Level Domain). You can also use "country:be" to search only pages from Belgium (based on ip rather than TLD). 
<img  src="img/sitehint.jpg" format="EPS" align="center"> </img> 
Search for pages inside one domain 
<img  src="img/sitehint2.jpg" format="EPS" align="center"> </img> 
Search for pages `not` containing some words. 
<img  src="img/negativehint.jpg" format="EPS" align="center"> </img> 


# Wikipedia 
Wikipedia is a web-based, free-content encyclopedia. Its growth over the past two years has been astonishing. You have a good chance of finding a clear explanation by typing your search term behind `http://en.wikipedia.org/wiki/`  http://en.wikipedia.org/wiki/   like this example shows. 
<img  src="img/wikipedia.jpg" format="EPS" align="center"> </img> 


# The Linux Documentation Project 
You can find much documentation, faqs, howtos and man pages about Linux and many of the programs running on Linux on `www.tldp.org`  http://www.tldp.org   . 


# Red Hat 
Red Hat has a lot of info online at `http://www.redhat.com/docs/manuals/`  http://www.redhat.com/docs/manuals/   in both pdf and html format. These manuals are good, but unfortunately are not always up to date. 


# Ubuntu 
Help for every Ubuntu release is available at `https://help.ubuntu.com`  https://help.ubuntu.com  . Ubuntu also has video of how to perform tasks on Ubuntu at `http://screencasts.ubuntu.com`  http://screencasts.ubuntu.com  . 


# linux-training.be 
This book is available for free in `.pdf` and `.html`. Download it at `http://linux-training.be`  http://www.vaiolabs.io   and learn more about Linux fundamentals, system administration, networking, storage, security and more. 



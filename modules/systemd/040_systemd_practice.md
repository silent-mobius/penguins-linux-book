
---

### Practice: systemd 

1. Determine on which target you are at the moment 
2. List all systemctl units with type of service 
3. Check what is the status of cron service. 
4. Disable cron service 
5. On RedHat based system, disable network-manager and enable networking service. 
6. On Debian based system, disable network-manager and enable netconf serice 
7. Use one command, to enable and start cron service 

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>


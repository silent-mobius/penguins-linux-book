
---

### Solution : systemd 

1. determine on which target you are at the moment 
```sh
systemctl get-default
```
runlevel command should also work, but command above provides 

2. list all systemctl units with type of service 
```sh
systemctl -t service
```

3. check what is the status of cron service. 
```sh     
systemctl status cron.service
```

4. disable cron service 
```sh
systemctl disable cron.service
```

5. on RedHat based system, disable network-manager and enable networking service. 
```sh
systemctl disable NetworkManager
systemctl enable networking      
```

6. on Debian based system, disable network-manager and enable netconf service 
```sh
systemctl disable NetworkManager
systemctl enable netconf
```

7. use one command, to enable and start cron service 

```sh
systemctl enable --now cron
```


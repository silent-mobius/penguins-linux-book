
---
### About System 

Many Unix and Linux distributions have been using `init.d` system to start daemons in the same way
that `Unix System V`  System V did back in 1983. 
In 2015, RedHat dropped support for `init.d` and considered it as legacy, while preferring to move to `systemd`. 
Most popular Linux distributions, such as Arch, Ubuntu and OpenSuse followed RedHat's act, and have migrated to `systemd`.
Debian have remained `systemd` with name `init`, yet underneath it uses `systemd`.
This chapter explains how to manage Linux with `systemd`. 


<img src="/home/aschapelle/Projects/linux-training/img/systemd_diagram.png" alt="systemd diagram" style="float:right;width:600px;"> </img>  


<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>
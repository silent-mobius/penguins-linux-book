`This chapter describes and shows a Linux distributions installation on Virtualbox. 
Later you will find Rocky8 and Debian11 distro's install chapters, which will guide you through the process of install.` 
This book assumes you have access to a working computer with network access.
Most companies have one or more Linux servers, if you have already logged on to it, 
then you 're all set so you may skip those chapters and go to the next one. 
Another option is to install Linux distribution with usb drive on your computer with or without
Microsoft Windows and follow the installation. Linux can help to create and resize 
partitions with setup a menu at boot time to choose Windows or Linux. 
If you do not have access to a computer with internet access at the moment, 
and if you are unable or unsure about installing Linux on your computer,
then this chapter proposes a third option: installing Linux in a virtual machine. 
Virtual machine is piece of software that based on your computers processor capabality, called `hypervisor`,
can emulate computer hardware. With that emulated hardware we can create additional operational system. 
Installation in a virtual machine based on `Virtualbox`, which is OpenSource,
is easy and safe. One of the great advantages of virtual machine, is the possibility of practice and capability of restating from scratch without 
damaging or formating your actual computer. 
The next chapter describes installation steps with screenshots to get a working Linux server 
in a `Virtualbox` based virtual machine. The steps are very similar to installing any Linux distributions such as Fedora, Debian,
and so on.
In case you have access to other hypervisor such as VMWare or Parallel instead of Virtualbox, 
you are welcome to use it, yet in case of any issue, you will
have to handle those issues alone. 

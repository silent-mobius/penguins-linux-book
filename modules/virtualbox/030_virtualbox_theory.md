---

# Download a Linux ISO image 
Lets start by downloading a Linux image ,an .ISO extention file, from the distribution of your choice from the Internet. While selecting the distribution,
please carry attention to the cpu architecture that you are using.
Usually cpu architecture of most modern cpus are `amd64` or  `x86_64` which is essentially the same. 
Choosing the wrong cpu architecture such as x86, arm, ppc or others, will almost immediately fail to boot the process. 

<img  src="img/vm1_download.png" format="EPS" align="center"> </img> 


# Download Virtualbox 
Step two is to download `Virtualbox` software. If you are currently running on Microsoft Windows or Mac, then download and install appropriate version of Virtualbox for Windows or Mac 

<img  src="img/vm2_download.png" format="EPS" align="center"> </img> 


---

# Create a virtual machine 
Now, lets start Virtualbox. Contrary to the screenshot below, your left pane should be empty. 

<img  src="img/vm3_virtualbox.png" format="EPS" align="center"> </img> 
Click `New` button to create a new virtual machine. Virtualbox will start a wizard to help us to setup the VM.
The screenshots below are taken on Mac OSX; they will be slightly different if you are running Microsoft Windows. 

<img  src="img/vm_wizard1.png" format="EPS" align="center"> </img> 
---

Name your virtual machine any thing your like and remember to choose cpu architecture correctly: select 32-bit in case you have downloaded 32bit ISO
or 64-bit if have downloaded 64bit. 

<img  src="img/vm_wizard2.png" format="EPS" align="center"> </img> 

Like any PC, we need to give our virtual machine random access memory, which in most of the cases is suggested to be 512MB for minimal server.
if you enough RAM on your physical hardware, then you can add at least 4GB or more. In othercase, you can stick to 256 or 512 MB. 

<img  src="img/vm_wizard3.png" format="EPS" align="center"> </img> 
---

Now, we need to create and select a new harddisk, yet remeber that this will be a virtual disk, thus it will be represnted as a file. 

<img  src="img/vm_wizard4.png" format="EPS" align="center"> </img> 

As for the question below, which type of drive it should be, please choose `vdi`. 

<img  src="img/vm_wizard5.png" format="EPS" align="center"> </img> 
---

In case you are using ssd or nvme drive on your physical drive, it is suggested `dynamically allocated`.
unless, you have old magnetic drive	such as sata 5400 rpm drive, or older, it is suggested to use, `fixed size`
which is also, useful in production. 

<img  src="img/vm_wizard6.png" format="EPS" align="center"> </img> 

When it comes to size, you are welcome to choose between 10GB and 16GB as the disk size, which is more than enough for our purpose. 

<img  src="img/vm_wizard7.png" format="EPS" align="center"> </img> 
---

Click `Create` to create the virtual disk. 

<img  src="img/vm_wizard8.png" format="EPS" align="center"> </img> 

Press `Create` to create the virtual machine. 

<img  src="img/vm_wizard9.png" format="EPS" align="center"> </img> 


---

# Attaching the .ISO image 
Up until now, we have created virtual machine and have downloaded .ISO file to Linux distribution. 
Before we start the virtual computer, we need to attach .ISO file to virtual drive reader and boot the .ISO file to install Linux.
let us take a look at some settings, press `Settings`. 

<img  src="img/vm_settings1.png" format="EPS" align="center"> </img> 

Do not worry if your screen looks different, just find the button named `storage`. 

<img  src="img/vm_settings2.png" format="EPS" align="center"> </img> 
---

Remember the .ISO file you downloaded? Connect this .ISO file to this virtual machine by clicking on the CD icon next to `Empty`. 

<img  src="img/vm_settings4.png" format="EPS" align="center"> </img> 

Now click on the other CD icon and attach your ISO file to this virtual CD drive. 

<img  src="img/vm_settings5.png" format="EPS" align="center"> </img> 
---

Verify that your download is accepted. If Virtualbox complains at this point, then you probably did not finish the download of the CD (try downloading it again). 

<img  src="img/vm_settings6.png" format="EPS" align="center"> </img> 

It could be useful to set the network adapter to bridge instead of NAT. Bridged usually will connect your virtual computer to the Internet. 

<img  src="img/vm_settings7.png" format="EPS" align="center"> </img> 


---

# install Linux 
The virtual machine is now ready to start. When given a choice at boot,
select `install` and follow the instructions on the screen. 
When the installation is finished, you can log on to the machine and start practising Linux. 



---

# solution: groups 
1. Create the groups tennis, football and sports. 
```sh
groupadd tennis ; groupadd football ; groupadd sports```
2. In one command, make venus a member of tennis and sports. 
```sh
usermod -a -G tennis,sports venus```
3. Rename the football group to foot. 
```sh
groupmod -n foot football```
4. Use vi to add serena to the tennis group. 
```sh
vi /etc/group```
5. Use the id command to verify that serena is a member of tennis. 
```sh
id (and after logoff logon serena should be member)```
6. Make someone responsible for managing group membership of foot and sports. Test that it works. 
```sh
gpasswd -A (to make manager)```
```sh
gpasswd -a (to add member)```



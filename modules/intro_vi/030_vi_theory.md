---

# command mode and insert mode 
The vi editor starts in `command mode`  command mode(vi)  . In command mode, you can type commands. Some commands will bring you into `insert mode`  insert mode(vi)  . In insert mode, you can type text. The `escape key` will return you to command mode. 
<table frame='all'>## getting to command mode 
<?dbfo table-width="80%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="5*" align="left"/>
<thead>
<row>
<entry>key</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>Esc</entry>
<entry>set vi(m) in command mode.</entry>
</row>
</tbody>
</tgroup>
</table>


# start typing (a A i I o O) 
The difference between a A i I o and O is the location where you can start typing. a will append after the current character and A will append at the end of the line. i will insert before the current character and I will insert at the beginning of the line. o will put you in a new line after the current line and O will put you in a new line before the current line. 
<table frame='all'>## switch to insert mode 
<?dbfo table-width="80%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="5*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>a</entry>
<entry>start typing after the current character</entry>
</row>
<row>
<entry>A</entry>
<entry>start typing at the end of the current line</entry>
</row>
<row>
<entry>i</entry>
<entry>start typing before the current character</entry>
</row>
<row>
<entry>I</entry>
<entry>start typing at the start of the current line</entry>
</row>
<row>
<entry>o</entry>
<entry>start typing on a new line after the current line</entry>
</row>
<row>
<entry>O</entry>
<entry>start typing on a new line before the current line</entry>
</row>
</tbody>
</tgroup>
</table>


---

# replace and delete a character (r x X) 
When in command mode (it doesn't hurt to hit the escape key more than once) you can use the x key to delete the current character. The big X key (or shift x) will delete the character left of the cursor. Also when in command mode, you can use the r key to replace one single character. The r key will bring you in insert mode for just one key press, and will return you immediately to command mode. 
<table frame='all'>## replace and delete 
<?dbfo table-width="90%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="4*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>x</entry>
<entry>delete the character below the cursor</entry>
</row>
<row>
<entry>X</entry>
<entry>delete the character before the cursor</entry>
</row>
<row>
<entry>r</entry>
<entry>replace the character below the cursor</entry>
</row>
<row>
<entry>p</entry>
<entry>paste after the cursor (here the last deleted character)</entry>
</row>
<row>
<entry>xp</entry>
<entry>switch two characters</entry>
</row>
</tbody>
</tgroup>
</table>


# undo, redo and repeat (u .) 
When in command mode, you can undo your mistakes with u. Use `ctrl-r` to redo the undo. 
You can do your mistakes twice with . (in other words, the . will repeat your last command). 
<table frame='all'>## undo and repeat 
<?dbfo table-width="80%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="5*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>u</entry>
<entry>undo the last action</entry>
</row>
<row>
<entry>ctrl-r</entry>
<entry>redo the last undo</entry>
</row>
<row>
<entry>.</entry>
<entry>repeat the last action</entry>
</row>
</tbody>
</tgroup>
</table>


# cut, copy and paste a line (dd yy p P) 
When in command mode, dd will cut the current line. yy will copy the current line. You can paste the last copied or cut line after (p) or before (P) the current line. 
<table frame='all'>## cut, copy and paste a line 
<?dbfo table-width="80%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="5*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>dd</entry>
<entry>cut the current line</entry>
</row>
<row>
<entry>yy</entry>
<entry>(yank yank) copy the current line</entry>
</row>
<row>
<entry>p</entry>
<entry>paste after the current line</entry>
</row>
<row>
<entry>P</entry>
<entry>paste before the current line</entry>
</row>
</tbody>
</tgroup>
</table>


---

# cut, copy and paste lines (3dd 2yy) 
When in command mode, before typing dd or yy, you can type a number to repeat the command a number of times. Thus, 5dd will cut 5 lines and 4yy will copy (yank) 4 lines. That last one will be noted by vi in the bottom left corner as "4 line yanked". 
<table frame='all'>## cut, copy and paste lines 
<?dbfo table-width="80%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="5*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>3dd</entry>
<entry>cut three lines</entry>
</row>
<row>
<entry>4yy</entry>
<entry>copy four lines</entry>
</row>
</tbody>
</tgroup>
</table>


# start and end of a line (0 or &#094; and $) 
When in command mode, the 0 and the caret &#094; will bring you to the start of the current line, whereas the $ will put the cursor at the end of the current line. You can add 0 and $ to the d command, d0 will delete every character between the current character and the start of the line. Likewise d$ will delete everything from the current character till the end of the line. Similarly y0 and y$ will yank till start and end of the current line. 
<table frame='all'>## start and end of line 
<?dbfo table-width="80%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="5*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>0</entry>
<entry>jump to start of current line</entry>
</row>
<row>
<entry>&#094;</entry>
<entry>jump to start of current line</entry>
</row>
<row>
<entry>$</entry>
<entry>jump to end of current line</entry>
</row>
<row>
<entry>d0</entry>
<entry>delete until start of line</entry>
</row>
<row>
<entry>d$</entry>
<entry>delete until end of line</entry>
</row>
</tbody>
</tgroup>
</table>


# join two lines (J) and more 
When in command mode, pressing `J` will append the next line to the current line. With `yyp` you duplicate a line and with `ddp` you switch two lines. 
<table frame='all'>## join two lines 
<?dbfo table-width="80%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="5*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>J</entry>
<entry>join two lines</entry>
</row>
<row>
<entry>yyp</entry>
<entry>duplicate a line</entry>
</row>
<row>
<entry>ddp</entry>
<entry>switch two lines</entry>
</row>
</tbody>
</tgroup>
</table>


---

# words (w b) 
When in command mode, `w` will jump to the next word and `b` will move to the previous word. w and b can also be combined with d and y to copy and cut words (dw db yw yb). 
<table frame='all'>## words 
<?dbfo table-width="80%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="5*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>w</entry>
<entry>forward one word</entry>
</row>
<row>
<entry>b</entry>
<entry>back one word</entry>
</row>
<row>
<entry>3w</entry>
<entry>forward three words</entry>
</row>
<row>
<entry>dw</entry>
<entry>delete one word</entry>
</row>
<row>
<entry>yw</entry>
<entry>yank (copy) one word</entry>
</row>
<row>
<entry>5yb</entry>
<entry>yank five words back</entry>
</row>
<row>
<entry>7dw</entry>
<entry>delete seven words</entry>
</row>
</tbody>
</tgroup>
</table>


# save (or not) and exit (:w :q :q! ) 
Pressing the colon : will allow you to give instructions to vi (technically speaking, typing the colon will open the `ex` editor). `:w` will write (save) the file, `:q` will quit an unchanged file without saving, and `:q!` will quit vi discarding any changes. `:wq` will save and quit and is the same as typing `ZZ` in command mode. 
<table frame='all'>## save and exit vi 
<?dbfo table-width="80%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="4*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>:w</entry>
<entry>save (write)</entry>
</row>
<row>
<entry>:w fname</entry>
<entry>save as fname</entry>
</row>
<row>
<entry>:q</entry>
<entry>quit</entry>
</row>
<row>
<entry>:wq</entry>
<entry>save and quit</entry>
</row>
<row>
<entry>ZZ</entry>
<entry>save and quit</entry>
</row>
<row>
<entry>:q!</entry>
<entry>quit (discarding your changes)</entry>
</row>
<row>
<entry>:w!</entry>
<entry>save (and write to non-writable file!)</entry>
</row>
</tbody>
</tgroup>
</table>
The last one is a bit special. With `:w!` `vi` will try to `chmod`  chmod(1)   the file to get write permission (this works when you are the owner) and will `chmod` it back when the write succeeds. This should always work when you are root (and the file system is writable). 


# Searching (/ ?) 
When in command mode typing / will allow you to search in vi for strings (can be a regular expression). Typing /foo will do a forward search for the string foo and typing ?bar will do a backward search for bar. 
<table frame='all'>## searching 
<?dbfo table-width="90%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="4*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>/string</entry>
<entry>forward search for string</entry>
</row>
<row>
<entry>?string</entry>
<entry>backward search for string</entry>
</row>
<row>
<entry>n</entry>
<entry>go to next occurrence of search string</entry>
</row>
<row>
<entry>/^string</entry>
<entry>forward search string at beginning of line</entry>
</row>
<row>
<entry>/string$</entry>
<entry>forward search string at end of line</entry>
</row>
<row>
<entry>/br[aeio]l</entry>
<entry>search for bral brel bril and brol</entry>
</row>
<row>
<entry>/\&#060;he\&#062;</entry>
<entry>search for the word `he` (and not for `he`re or t`he`)</entry>
</row>
</tbody>
</tgroup>
</table>


# replace all ( &#058;1,$ s&#047;foo&#047;bar&#047;g ) 
To replace all occurrences of the string foo with bar, first switch to ex mode with &#058; . Then tell vi which lines to use, for example 1,&#036; will do the replace all from the first to the last line. You can write 1,5 to only process the first five lines. The s&#047;foo&#047;bar&#047;g will replace all occurrences of foo with bar. 
<table frame='all'>## replace 
<?dbfo table-width="90%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="center"/>
<colspec colname='c2' colwidth="5*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>:4,8 s/foo/bar/g</entry>
<entry>replace foo with bar on lines 4 to 8</entry>
</row>
<row>
<entry>:1,$ s/foo/bar/g</entry>
<entry>replace foo with bar on all lines</entry>
</row>
</tbody>
</tgroup>
</table>


# reading files (:r :r !cmd) 
When in command mode, :r foo will read the file named foo, :r !foo will execute the command foo. The result will be put at the current location. Thus :r !ls will put a listing of the current directory in your text file. 
<table frame='all'>## read files and input 
<?dbfo table-width="80%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="5*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>:r fname</entry>
<entry>(read) file fname and paste contents</entry>
</row>
<row>
<entry>:r !cmd</entry>
<entry>execute cmd and paste its output</entry>
</row>
</tbody>
</tgroup>
</table>


# text buffers 
There are 36 buffers in vi to store text. You can use them with the `"` character. 
<table frame='all'>## text buffers 
<?dbfo table-width="80%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="5*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>"add</entry>
<entry>delete current line and put text in buffer a</entry>
</row>
<row>
<entry>"g7yy</entry>
<entry>copy seven lines into buffer g</entry>
</row>
<row>
<entry>"ap</entry>
<entry>paste from buffer a</entry>
</row>
</tbody>
</tgroup>
</table>


# multiple files 
You can edit multiple files with vi. Here are some tips. 
<table frame='all'>## multiple files 
<?dbfo table-width="80%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="center"/>
<colspec colname='c2' colwidth="4*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>vi file1 file2 file3</entry>
<entry>start editing three files</entry>
</row>
<row>
<entry>:args</entry>
<entry>lists files and marks active file</entry>
</row>
<row>
<entry>:n</entry>
<entry>start editing the next file</entry>
</row>
<row>
<entry>:e</entry>
<entry>toggle with last edited file</entry>
</row>
<row>
<entry>:rew</entry>
<entry>rewind file pointer to first file</entry>
</row>
</tbody>
</tgroup>
</table>


# abbreviations 
With `:ab` you can put abbreviations in vi. Use `:una` to undo the abbreviation. 
<table frame='all'>## abbreviations 
<?dbfo table-width="80%" ?>
<tgroup cols='2' align='center' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="4*" align="center"/>
<colspec colname='c2' colwidth="7*" align="left"/>
<thead>
<row>
<entry>command</entry>
<entry>action</entry>
</row>
</thead>
<tbody>
<row>
<entry>:ab str long string</entry>
<entry>abbreviate `str` to be 'long string'</entry>
</row>
<row>
<entry>:una str</entry>
<entry>un-abbreviate str</entry>
</row>
</tbody>
</tgroup>
</table>


---

# key mappings 
Similarly to their abbreviations, you can use mappings with `:map` for command mode and `:map!` for insert mode. 
This example shows how to set the F6 function key to toggle between `set number` and `set nonumber`. The &#060;bar&#062; separates the two commands, `set number!` toggles the state and `set number?` reports the current state. 
```sh
:map &#060;F6&#062; :set number!&#060;bar&#062;set number?&#060;CR&#062;```


# setting options 
Some options that you can set in vim. 
```sh

:set number  ( also try :se nu )
:set nonumber
:syntax on
:syntax off
:set all  (list all options)
:set tabstop=8
:set tx   (CR/LF style endings)
:set notx
```
You can set these options (and much more) in `~/.vimrc`  .vimrc   for vim or in `~/.exrc`  .exrc   for standard vi. 
```sh

paul@barry:~$ cat ~/.vimrc
syntax on
set number
set tabstop=4
set textwidth=78
map &#060;F6&#062; :set number!&#060;bar&#062;set number?&#060;CR&#062;
paul@barry:~$
```



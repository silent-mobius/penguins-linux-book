---

# solution: vi(m) 
1. Create new file with vim and in it write down your creadentials: name, lastname, id, address, favorite OS, favorite programming language 
2. Copy the within vi and save the data with different name. 
3. In the first file, what 3 key sequence in command mode will duplicate your current line. 
```sh
yyp```
4. In the first file, what 3 key sequence in command mode will switch two lines' place (line five becomes line six and line six becomes line five). 
```sh
ddp```
5. In the first file, what 2 key sequence in command mode will switch a character's place with the next one. 
```sh
xp```
6. Vi can understand macro's. A macro can be recorded with q followed by the name of the macro. So qa will record the macro named a. Pressing q again will end the recording. You can recall the macro with @ followed by the name of the macro. Try this example: i 1 'Escape Key' qa yyp 'Ctrl a' q 5@a (Ctrl a will increase the number with one). 
7. Copy /etc/passwd to your ~/passwd. Open the last one in vi and press Ctrl v. Use the arrow keys to select a Visual Block, you can copy this with y or delete it with d. Try pasting it. 
```sh
cp /etc/passwd ~
vi passwd
(press Ctrl-V)```
8. What does dwwP do when you are at the beginning of a word in a sentence ? 
`dwwP` can switch the current word with the next word. 
9. Start the vimtutor and do some or all of the exercises. You might need to run `apt install vim` on Debian in order to install it. 
```sh
vimtutor```



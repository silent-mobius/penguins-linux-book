---

# solution: working with directories 
1. Display your current directory. 
```sh
pwd```	
2. Change to the /etc directory. 
```sh
cd /etc```	
3. Now change to your home directory using only three key presses. 
```sh
cd (and the enter key)```	
4. Change to the /boot/grub directory using only eleven key presses. 
```sh
cd /boot/grub (use the tab key)```	
5. Go to the parent directory of the current directory. 
```sh
cd .. (with space between cd and ..)```	
6. Go to the root directory. 
```sh
cd /```	
7. List the contents of the root directory. 
```sh
ls```	
8. List a long listing of the root directory. 
```sh
ls -l```
9. Stay where you are, and list the contents of /etc. 
```sh
ls /etc```	
10. Stay where you are, and list the contents of /bin and /sbin. 
```sh
ls /bin /sbin```	
11. Stay where you are, and list the contents of &#126;. 
```sh
ls &#126;```	
12. List all the files (including hidden files) in your home directory. 
```sh
ls -al &#126;```	
13. List the files in /boot in a human readable format. 
```sh
ls -lh /boot```	
14. Create a directory testdir in your home directory. 
```sh
mkdir &#126;/testdir```	
15. Change to the /etc directory, stay here and create a directory newdir in your home directory. 
```sh
cd /etc &#59; mkdir &#126;/newdir```	
16. Create in one command the directories &#126;/dir1/dir2/dir3 (dir3 is a subdirectory from dir2, and dir2 is a subdirectory from dir1 ). 
```sh
mkdir -p &#126;/dir1/dir2/dir3```	
17. Remove the directory testdir. 
```sh
rmdir testdir```	
18. If time permits (or if you are waiting for other students to finish this practice), use and understand `pushd` and `popd`. Use the man page of `bash` to find information about these commands. 
```sh
man bash           # opens the manual
/pushd             # searches for pushd
n                  # next (do this two/three times)```

The Bash shell has two built-in commands called `pushd`  pushd   and `popd`  popd  . Both commands work with a common stack of previous directories. Pushd adds a directory to the stack and changes to a new current directory, popd removes a directory from the stack and sets the current directory. 
```sh
paul@debian10:/etc$ `cd /bin`
paul@debian10:/bin$ `pushd /lib`
/lib /bin
paul@debian10:/lib$ `pushd /proc`
/proc /lib /bin
paul@debian10:/proc$ `popd`
/lib /bin
paul@debian10:/lib$ `popd`
/bin```



<abstract>
This book is meant to be used in an instructor-led training. For self-study, the intent is to read this book next to a working Linux computer so you can immediately do every subject, practicing each command. 
This book is aimed at novice Linux developers or system administrators (and might be interesting and useful for home users that want to know a bit more about their Linux system). However, this book is not meant as an introduction to Linux desktop applications like text editors, browsers, mail clients, multimedia or office applications. 
More information and free .pdf available at `http://linux-training.be`. 

ISBN 978-1-62154-837-9 
Cover design by Inge Mestdagh, Tux mascotte created by Larry Ewing and The GIMP 


Feel free to contact the author:
AUTHORSCONTACT


Contributors to the Linux Training project are:
CONTRIBUTORS


We'd also like to thank our reviewers: 
REVIEWERS










`Copyright 2007-YEAR [COPYRIGHTS]` 
</abstract>

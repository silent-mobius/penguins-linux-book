<abstract>

This book is meant to be used in an instructor-led training. For self-study, the intent is to read this book next to a working Linux computer so you can immediately do every subject, practicing each command. 
This book is aimed at novice Linux system administrators (and might be interesting and useful for home users that want to know a bit more about their Linux system). However, this book is not meant as an introduction to Linux desktop applications like text editors, browsers, mail clients, multimedia or office applications. 
More information and free .pdf available at `http://linux-training.be` . 

Feel free to contact the author:
AUTHORSCONTACT


Contributors to the Linux Training project are:
CONTRIBUTORS


We'd also like to thank our reviewers: 
REVIEWERS



Copyright 2007-YEAR [COPYRIGHTS] 
Permission is granted to copy, distribute and/or modify this document under the terms of the `GNU Free Documentation License`, Version 1.3 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license is included in the section entitled 'GNU Free Documentation License'. 

</abstract>

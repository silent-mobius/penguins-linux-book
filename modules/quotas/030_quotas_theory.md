# About Disk Quotas 
To limit the disk space used by user, you can set up `disk quotas`  quota's  . This requires adding `usrquota`  usrquota   and/or `grpquota`  grpquota   to one or more of the file systems in `/etc/fstab`  /etc/fstab  .  
```sh

root@RHELv8u4:~# cat /etc/fstab | grep usrquota
/dev/VolGroup00/LogVol02     /home     ext3     usrquota,grpquota   0 0
```
Next you need to remount the file system. 
```sh

root@RHELv8u4:~# mount -o remount /home
```
The next step is to build the `quota.user`  quota.user   and/or `quota.group`  quota.group   files. These files (called the `quota files`) contain the table of the disk usage on that file system. Use the `quotacheck`  quotacheck(1)   command to accomplish this. 
```sh

root@RHELv8u4:~# quotacheck -cug /home
root@RHELv8u4:~# quotacheck -avug
```
The `-c` is for create, `u` for user quota, `g` for group, `a` for checking all quota enabled file systems in /etc/fstab and `v` for verbose information. The next step is to edit individual user quotas with `edquota`  edquota(1)   or set a general quota on the file system with `edquota -t`. The tool will enable you to put `hard` (this is the real limit) and `soft` (allows a grace period) limits on `blocks` and `inodes`. The `quota`  quota(1)   command will verify that quota for a user is set. You can have a nice overview with `repquota`  repquota(1)  . 
The final step (before your users start complaining about lack of disk space) is to enable quotas with `quotaon(1)`  quotaon(1)  . 
```sh
root@RHELv8u4:~# quotaon -vaug```
Issue the `quotaoff`  quotaoff(1)   command to stop all complaints. 
```sh
root@RHELv8u4:~# quotaoff -vaug```


# Practice Disk quotas 
1. Implement disk quotas on one of your new partitions. Limit one of your users to 10 megabyte. 
2. Test that they work by copying many files to the quota'd partition. 	



---

# Practice: packet forwarding 
0. You have the option to select (or create) an internal network when adding a network card in `VirtualBox`  virtualbox   or `VMWare`  vmware  . Use this option to create two internal networks. I named them `leftnet` and `rightnet`, but you can choose any other name. 
<img  src="img/LAN_networks.png" format="EPS" align="center"> </img>  
1. Set up two Linux machines, one on `leftnet`, the other on `rightnet`. Make sure they both get an ip-address in the correct subnet. These two machines will be 'left' and 'right' from the 'router'. 
<img  src="img/leftnet_rightnet_router2.png" format="EPS" align="center"> </img>  
2. Set up a third Linux computer with three network cards, one on `leftnet`, the other on `rightnet`. This computer will be the 'router'. Complete the table below with the relevant names, ip-addresses and `mac-addresses`  mac address  . 
<table frame='all'>## Packet Forwarding Exercise 
<?dbfo table-width="100%" ?>
<tgroup cols='5' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="left"/>
<colspec colname='c2' colwidth="3*" align="left"/>
<colspec colname='c3' colwidth="3*" align="left"/>
<colspec colname='c4' colwidth="3*" align="left"/>
<colspec colname='c5' colwidth="3*" align="left"/>
<thead>
<row>
<entry></entry>
<entry>leftnet computer</entry>
<entry namest="c2" nameend="c3">the router</entry>
<entry>rightnet computer</entry>
</row>
</thead>
<tbody>
<row>
<entry>MAC</entry>
<entry></entry>
<entry></entry>
<entry></entry>
<entry></entry>
</row>
<row>
<entry>IP</entry>
<entry></entry>
<entry></entry>
<entry></entry>
<entry></entry>
</row>
</tbody>
</tgroup>
</table>
3. How can you verify whether the `router` will allow packet forwarding by default or not ? Test that you can `ping`  ping   from the `router` to the two other machines, and from those two machines to the `router`. Use `arp -a` to make sure you are connected with the correct `mac addresses`. 
---

4. `Ping`  ping   from the leftnet computer to the rightnet computer. Enable and/or disable packet forwarding on the `router` and verify what happens to the ping between the two networks. If you do not succeed in pinging between the two networks (on different subnets), then use a sniffer like `wireshark` or `tcpdump` to discover the problem. 
5. Use `wireshark`  wireshark   or `tcpdump`  tcpdump   -xx to answer the following questions. Does the source MAC change when a packet passes through the filter ? And the destination MAC ? What about source and destination IP-addresses ? 
6. Remember the third network card on the router ? Connect this card to a LAN with internet connection. On many LAN's the command `dhclient eth0`  dhclient   just works (replace `eth0` with the correct interface). 
```sh
root@router~# dhclient eth0```
You now have a setup similar to this picture. What needs to be done to give internet access to `leftnet` and `rightnet`. 
<img  src="img/leftnet_rightnet_router3.png" format="EPS" align="center"> </img>  



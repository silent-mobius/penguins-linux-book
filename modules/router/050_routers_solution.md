---

# solution: packet forwarding 
<img  src="img/LAN_networks.png" format="EPS" align="center"> </img>  
1. Set up two Linux machines, one on `leftnet`, the other on `rightnet`. Make sure they both get an ip-address in the correct subnet. These two machines will be 'left' and 'right' from the 'router'. 
<img  src="img/leftnet_rightnet_router2.png" format="EPS" align="center"> </img>  
The ip configuration on your computers should be similar to the following two screenshots. Both machines must be in a different subnet (here 192.168.60.0/24 and 192.168.70.0/24). I created a little script on both machines to configure the interfaces. 
```sh
root@left~# cat leftnet.sh
pkill dhclient
ifconfig eth0 192.168.60.8 netmask 255.255.255.0```
```sh
root@right~# cat rightnet.sh
pkill dhclient
ifconfig eth0 192.168.70.9 netmask 255.255.255.0```
2. Set up a third Linux computer with three network cards, one on `leftnet`, the other on `rightnet`. This computer will be the 'router'. Complete the table below with the relevant names, ip-addresses and mac-addresses. 
```sh
root@router~# cat router.sh
ifconfig eth1 192.168.60.1 netmask 255.255.255.0
ifconfig eth2 192.168.70.1 netmask 255.255.255.0
#echo 1 > /proc/sys/net/ipv4/ip_forward```
Your setup may use different ip and mac addresses than the ones in the table below. 
<table frame='all'>## Packet Forwarding Solution 
<?dbfo table-width="100%" ?>
<tgroup cols='4' align='left' colsep='1' rowsep='1'>
<colspec colname='c2' colwidth="1*" align="center"/>
<colspec colname='c3' colwidth="1*" align="center"/>
<colspec colname='c4' colwidth="1*" align="center"/>
<colspec colname='c5' colwidth="1*" align="center"/>
<thead>
<row>
<entry>leftnet computer</entry>
<entry namest="c2" nameend="c3">the router</entry>
<entry>rightnet computer</entry>
</row>
</thead>
<tbody>
<row>
<entry>08:00:27:f6:ab:b9</entry>
<entry>08:00:27:43:1f:5a</entry>
<entry>08:00:27:be:4a:6b</entry>
<entry>08:00:27:14:8b:17</entry>
</row>
<row>
<entry>192.168.60.8</entry>
<entry>192.168.60.1</entry>
<entry>192.168.70.1</entry>
<entry>192.168.70.9</entry>
</row>
</tbody>
</tgroup>
</table>
3. How can you verify whether the `router` will allow packet forwarding by default or not ? Test that you can ping from the `router` to the two other machines, and from those two machines to the `router`. Use `arp -a` to make sure you are connected with the correct `mac addresses`. 
This can be done with "`grep ip_forward /etc/sysctl.conf`" (1 is enabled, 0 is disabled) or with `sysctl -a | grep ip_for`. 
```sh
root@router~# grep ip_for /etc/sysctl.conf 
net.ipv4.ip_forward = 0```
4. Ping from the leftnet computer to the rightnet computer. Enable and/or disable packet forwarding on the `router` and verify what happens to the ping between the two networks. If you do not succeed in pinging between the two networks (on different subnets), then use a sniffer like wireshark or tcpdump to discover the problem. 
Did you forget to add a `default gateway` to the LAN machines ? Use `route add default gw 'ip-address'`.  
```sh
root@left~# route add default gw 192.168.60.1```
```sh
root@right~# route add default gw 192.168.70.1```
You should be able to ping when packet forwarding is enabled (and both default gateways are properly configured). The ping will not work when packet forwarding is disabled or when gateways are not configured correctly. 
5. Use wireshark or tcpdump -xx to answer the following questions. Does the source MAC change when a packet passes through the filter ? And the destination MAC ? What about source and destination IP-addresses ? 
Both MAC addresses are changed when passing the router. Use `tcpdump -xx` like this: 
```sh
root@router~# tcpdump -xx -i eth1```
```sh
root@router~# tcpdump -xx -i eth2```
---

6. Remember the third network card on the router ? Connect this card to a LAN with internet connection. On many LAN's the command `dhclient eth0` just works (replace `eth0` with the correct interface. 
```sh
root@router~# dhclient eth0```
You now have a setup similar to this picture. What needs to be done to give internet access to `leftnet` and `rightnet`. 
<img  src="img/leftnet_rightnet_router3.png" format="EPS" align="center"> </img>  
The clients on `leftnet` and `rightnet` need a working `dns server`. We use one of Google's dns servers here.  
```sh
echo nameserver 8.8.8.8 > /etc/resolv.conf```



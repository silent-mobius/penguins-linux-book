---

# solution: getting started with samba 
1. Take a backup copy of the original smb.conf, name it smb.conf.orig 
```sh
cd /etc/samba ; cp smb.conf smb.conf.orig```
2. Enable SWAT and take a look at it. 
```sh
on Debian/Ubuntu: vi /etc/inetd.conf (remove # before swat)```
```sh
on RHEL/Fedora: vi /etc/xinetd.d/swat (set disable to no)```
3. Stop the Samba server. 
```sh
/etc/init.d/smb stop (Red Hat)```
```sh
/etc/init.d/samba stop (Debian)```
4. Create a minimalistic smb.conf.minimal and test it with testparm. 
```sh
cd /etc/samba ; mkdir my_smb_confs ; cd my_smb_confs```
```sh
vi smb.conf.minimal```
```sh
testparm smb.conf.minimal```
5. Use tesparm -s to create /etc/samba/smb.conf from your smb.conf.minimal . 
```sh
testparm -s smb.conf.minimal > ../smb.conf```
6. Start Samba with your minimal smb.conf. 
```sh
/etc/init.d/smb restart (Red Hat)```
```sh
/etc/init.d/samba restart (Debian)```
7. Verify with smbclient that your Samba server works. 
```sh
smbclient -NL 127.0.0.1```
8. Verify that another computer can see your Samba server. 
```sh
smbclient -NL 'ip-address' (on a Linux)```
9. Browse the network with net view, smbtree and with Windows Explorer. 
```sh
on Linux: smbtree```
```sh
on Windows: net view (and WindowsKey + e)```
10. Change the "Server String" parameter in smb.conf. How long does it take before you see the change (net view, smbclient, My Network Places,...) ? 
```sh
vi /etc/samba/smb.conf```
```sh
(should take only seconds when restarting samba)```
11. Will restarting Samba after a change to smb.conf speed up the change ? 
```sh
yes```
12. Which computer is the master browser master in your workgroup ? What is the master browser ? 
```sh
The computer that won the elections.```
```sh
This machine will make the list of computers in the network```
13. If time permits (or if you are waiting for other students to finish this practice), then install a sniffer (wireshark) and watch the browser elections. 
```sh
On ubuntu: sudo aptitude install wireshark```
```sh
then: sudo wireshark, select interface```



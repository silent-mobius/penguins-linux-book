Sometimes a server needs more than one `ip address` on the same network card, we call this `binding`  binding(ip)   ip addresses. 
Linux can also activate multiple network cards behind the same `ip address`, this is called `bonding`  bonding(ip)  . 
This chapter will teach you how to configure `binding` and `bonding` on the most common Linux distributions. 

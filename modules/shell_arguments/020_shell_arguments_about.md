 This chapter introduces us to `shell expansion` by taking a close look at `commands` and `arguments`.
 Knowing `shell expansion` is important because lots of `commands` on various Linux systems are processed and most likely changed by the 
 `shell` before they are executed. 
 The command line interface (cli) that enables us to communicate with Linux kernel is called `shell`. 
The shell history is long and goes back to end of 70's, to UNIX OS, which their own shell that was named `sh`  /bin/sh  ,
that was developed by `Stephen Bourne` at `Bell Labs`, it was a replacement for the Thompson shell, whose executable file had the same name:
 `sh`  /bin/sh  .
 Since then `sh`  /bin/sh   it has evolved, in several phases of C programming language shell also known as `csh` 
  /bin/csh  . later in 80's David Korn developed shell  adding a lot of features and shortly named it as 
 `ksh`  /bin/ksh  , yet also kept backwards compatability to old`sh`  /bin/sh  
 Most widely used `shell` on most Linux distributions is called `bash`  /bin/bash  , developed by Brian Fox for GNU project, 
 first released in 1989, it has been used as the default shell for most Linux distributions since 1989. By the way, Bash stands for `Bourne again shell`  Bourne again shell  ,
 because it incorporated all the features of `sh`, `ksh` and all based on POSIX standard. 

 This chapter frequently uses the `echo`  echo(1)   command to demonstrate shell features.
The `echo` command is very simple: it prints the standard input that it receives, back to standard output. 
 ```sh
paul@laika:~$ echo Burtonville
Burtonville
paul@laika:~$ echo Smurfs are blue
Smurfs are blue```

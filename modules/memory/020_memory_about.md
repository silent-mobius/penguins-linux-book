This chapter will tell you how to manage RAM memory and cache. 
We start with some simple tools to display information about memory: `free -om`  free(1)  , `top`  top(1)   and `cat /proc/meminfo`  /proc/meminfo  . 
We continue with managing swap space, using terms like `swapping`  swapping  , `paging`  paging   and `virtual memory`  virtual memory  . 
The last part is about using `vmstat` to monitor swap usage. 

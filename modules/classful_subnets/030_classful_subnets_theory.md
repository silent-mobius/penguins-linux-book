---

# ip address classes 
	 In the previous chapter we divided ip addresses into classes. 
<table frame='all'>## ip address classes 
<?dbfo table-width="80%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="1*" align="center"/>
<colspec colname='c2' colwidth="1*" align="left"/>
<colspec colname='c3' colwidth="2*" align="center"/>
<thead>
<row>
  <entry>class</entry>
  <entry>first bit(s)</entry>
  <entry>first decimal byte</entry>
</row>
</thead>
<tbody>
<row>
  <entry>A</entry>
  <entry>0</entry>
  <entry>0-127</entry>
</row>
<row>
  <entry>B</entry>
  <entry>10</entry>
  <entry>128-191</entry>
</row>
<row>
  <entry>C</entry>
  <entry>110</entry>
  <entry>192-223</entry>
</row>
<row>
  <entry>D</entry>
  <entry>1110</entry>
  <entry>224-239</entry>
</row>
<row>
  <entry>E</entry>
  <entry>1111</entry>
  <entry>240-255</entry>
</row>
</tbody>
</tgroup>
</table>


# classful subnet masks 
 The first three classes of ipv4 addresses each have a default `subnet mask`  subnet mask  . 
	 Class A addresses have a subnet with eight bits set to 1 followed by 24 bits set to zero.
Class B addresses have 16 bits set to 1 followed by 16 bits set to 0.
Class C addresses have 24 bits set to 1 followed by eight bits set to 0. This gives us the following tables. 
<table frame='all'>## default decimal subnet mask 
<?dbfo table-width="75%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='klasse' colwidth="3*" align="center"/>
<colspec colname='bits' colwidth="4*" align="center"/>
<colspec colname='subnet' colwidth="5*" align="center"/>
<thead>
<row>
  <entry>class</entry>
  <entry>bits set to 1</entry>
  <entry>default subnet mask</entry>
</row>
</thead>
<tbody>
<row>
  <entry>A</entry>
  <entry>8</entry>
  <entry>255.0.0.0</entry>
</row>
<row>
  <entry>B</entry>
  <entry>16</entry>
  <entry>255.255.0.0</entry>
</row>
<row>
  <entry>C</entry>
  <entry>24</entry>
  <entry>255.255.255.0</entry>
</row>
</tbody>
</tgroup>
</table>

<table frame='all'>## default binary subnet mask 
<?dbfo table-width="99%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='klasse' colwidth="1*" align="center"/>
<colspec colname='bits' colwidth="2*" align="center"/>
<colspec colname='subnet' colwidth="4*" align="center"/>
<thead>
<row>
  <entry>class</entry>
  <entry>bits set to 1</entry>
  <entry>default subnet mask</entry>
</row>
</thead>
<tbody>
<row>
  <entry>A</entry>
  <entry>8</entry>
  <entry>11111111.00000000.00000000.00000000</entry>
</row>
<row>
  <entry>B</entry>
  <entry>16</entry>
  <entry>11111111.11111111.00000000.00000000</entry>
</row>
<row>
  <entry>C</entry>
  <entry>24</entry>
  <entry>11111111.11111111.11111111.00000000</entry>
</row>
</tbody>
</tgroup>
</table>



---

# Practice default subnet masks 
 Write down the default subnet mask for the following ip addresses: 
<table frame='all'>## practice default subnet masks 
<?dbfo table-width="70%" ?>
<tgroup cols='2' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="3*" align="center"/>
<colspec colname='c2' colwidth="3*" align="center"/>
<thead>
<row>
  <entry>ip address</entry>
  <entry>default mask ?</entry>
</row>
</thead>
<tbody>
<row>
  <entry>192.168.42.33</entry>
  <entry></entry>
</row>
<row>
  <entry>9.101.12.01</entry>
  <entry>255.0.0.0</entry>
</row>
<row>
  <entry>188.33.42.33</entry>
  <entry></entry>
</row>
<row>
  <entry>9.42.12.33</entry>
  <entry></entry>
</row>
<row>
  <entry>230.19.4.42</entry>
  <entry></entry>
</row>
<row>
  <entry>11.19.6.200</entry>
  <entry></entry>
</row>
<row>
  <entry>191.192.193.194</entry>
  <entry></entry>
</row>
<row>
  <entry>134.0.0.42</entry>
  <entry></entry>
</row>
</tbody>
</tgroup>
</table>


---

# network id en host id 
	 Bij het uitvoeren van `ifconfig`  ifconfig(Unix)   op een Unix/Linux en `ipconfig`  ipconfig(MS)   op een MS Windows computer merkt je dat je steeds een `ip-adres`  ip-adres   en en `subnet mask`  subnet mask   krijgt. De combinatie van die twee bepaalt in welk netwerk een computer zich bevindt. 
	 Als het subnet mask gelijk is aan 255.0.0.0, dan vormt de eerste byte van het ip-adres aangevuld met nullen het `network id`  network id  . Bij 255.255.0.0 als subnet mask zijn er twee bytes (aangevuld met nullen) die het `network id` vormen. Bij 255.255.255.0 zijn er drie bytes (en een nul) die het `network id` vormen. 
	 De rest van het ip-adres is dan het `host id`. Het `network id` bepaalt het netwerk waarin een computer zich bevindt, het `host id`  host id   is uniek voor een host binnen het netwerk. 
	 Een overzichtje met voorbeelden: 
<table frame='all'>## network id en host id 
<?dbfo table-width="95%" ?>
<tgroup cols='4' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="2*" align="center"/>
<colspec colname='c2' colwidth="3*" align="center"/>
<colspec colname='c3' colwidth="2*" align="center"/>
<colspec colname='c4' colwidth="1*" align="center"/>
<thead>
<row>
  <entry>ip-adres</entry>
  <entry>default subnet mask</entry>
  <entry>network id</entry>
  <entry>host id</entry>
</row>
</thead>
<tbody>
<row>
  <entry>192.168.1.42</entry>
  <entry>255.255.255.0</entry>
  <entry>192.168.1.0</entry>
  <entry>42</entry>
</row>
<row>
  <entry>192.168.1.33</entry>
  <entry>255.255.255.0</entry>
  <entry>192.168.1.0</entry>
  <entry>33</entry>
</row>
<row>
  <entry>192.168.12.1</entry>
  <entry>255.255.255.0</entry>
  <entry>192.168.12.0</entry>
  <entry>1</entry>
</row>
<row>
  <entry>172.16.12.1</entry>
  <entry>255.255.0.0</entry>
  <entry>172.16.0.0</entry>
  <entry>12.1</entry>
</row>
<row>
  <entry>172.16.33.42</entry>
  <entry>255.255.0.0</entry>
  <entry>172.16.0.0</entry>
  <entry>33.42</entry>
</row>
<row>
  <entry>10.3.0.4</entry>
  <entry>255.0.0.0</entry>
  <entry>10.0.0.0</entry>
  <entry>3.0.4</entry>
</row>
<row>
  <entry>10.33.0.42</entry>
  <entry>255.0.0.0</entry>
  <entry>10.0.0.0</entry>
  <entry>33.0.42</entry>
</row>
</tbody>
</tgroup>
</table>


---

# oefening network id en host id 
	 1. Noteer de `network id` en `host id` voor de volgende ip-adressen. 
	 ```sh
192.168.42.42```
	 
	 
	 ```sh
9.8.7.6```
	 
	 
	 ```sh
42.42.42.42```
	 
	 
	 ```sh
169.254.42.1```
	 
	 
	 ```sh
191.42.17.18```
	 
	 
	 ```sh
193.42.17.18```
	 


---

# lokale computer of niet ? 
	 Wat is het nut van het kennen van een `network id`? Wel het `network id` bepaalt of een computer lokaal in je netwerk staat of niet. 
	 Indien je wil communiceren met een andere computer, dan heb je zijn `ip-adres` nodig. Als die computer dan op hetzelfde netwerk zit, dan doet jouw computer een `arp`  arp   om het `MAC adres`  MAC   van de andere computer te vinden. Als die computer echter op een ander netwerk zet, dan stuurt jouw computer het packetje naar de `router`. 
	 Alvorens over te gaan tot een `arp` (voor die andere computer of voor de router) zal jouw computer het `network id` van jouw computer vergelijken met dat van de andere computer. Indien gelijk, dan betreft het een computer op hetzelfde netwerk, indien verschillend, dan staat die computer achter de `router`  router  . 
	 Een overzichtje met voorbeelden (gebruik standaard subnet mask): 
<table frame='all'>## local or remote computer? 
<?dbfo table-width="98%" ?>
<tgroup cols='5' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' align="center"/>
<colspec colname='c2' align="center"/>
<colspec colname='c3' align="center"/>
<colspec colname='c4' align="center"/>
<colspec colname='c5' align="center"/>
<thead>
<row>
  <entry>computer A</entry>
  <entry>computer B</entry>
  <entry>network id A</entry>
  <entry>network id B</entry>
  <entry>lokaal ?</entry>
</row>
</thead>
<tbody>
<row>
  <entry>192.168.1.42</entry>
  <entry>192.168.1.33</entry>
  <entry>192.168.1.0</entry>
  <entry>192.168.1.0</entry>
  <entry>ja</entry>
</row>
<row>
  <entry>192.168.1.33</entry>
  <entry>192.168.12.1</entry>
  <entry>192.168.1.0</entry>
  <entry>192.168.12.0</entry>
  <entry>nee</entry>
</row>
<row>
  <entry>10.3.0.4</entry>
  <entry>10.33.0.42</entry>
  <entry>10.0.0.0</entry>
  <entry>10.0.0.0</entry>
  <entry>ja</entry>
</row>
</tbody>
</tgroup>
</table>


---

# oefening lokale computer of niet? 
	 1. Staan de volgende computers in hetzelfde netwerk ? 
	 ```sh
192.168.1.42 en 192.168.1.33```
	 
	 
	 ```sh
10.105.42.42 en 10.105.42.33```
	 
	 
	 ```sh
10.105.42.42 en 10.99.42.33```
	 
	 
	 ```sh
11.16.42.42 en 12.16.42.33```
	 
	 
	 ```sh
169.254.18.42 en 169.254.33.42```
	 
	 
	 ```sh
191.168.42.42 en 191.168.33.33```
	 
	 
	 ```sh
9.1.2.3 en 9.123.234.42```
	 
	 


---

# subnet notatie 
	 We kunnen de notatie van `ip-adres/subnet mask`  cidr   afkorten als we voor de subnet mask enkel het aantal bits vernoemen dat op 1 staat. Zo wordt 255.0.0.0 gelijk aan /8, wordt 255.255.0.0 gelijk aan /16 en 255.255.255.0 gelijk aan /24. 
 Afgekort kunnen we dus 192.168.1.42/24 schrijven i.p.v. 192.168.1.42 met subnet mask 255.255.255.0 . 
<table frame='all'>## cidr notatie 
<?dbfo table-width="80%" ?>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<colspec colname='klasse' colwidth="1*" align="center"/>
<colspec colname='bits' colwidth="2*" align="center"/>
<colspec colname='subnet' colwidth="1*" align="center"/>
<thead>
<row>
  <entry>klasse</entry>
  <entry>default subnet mask</entry>
  <entry>notatie</entry>
</row>
</thead>
<tbody>
<row>
  <entry>A</entry>
  <entry>255.0.0.0</entry>
  <entry>/8</entry>
</row>
<row>
  <entry>B</entry>
  <entry>255.255.0.0</entry>
  <entry>/16</entry>
</row>
<row>
  <entry>C</entry>
  <entry>255.255.255.0</entry>
  <entry>/24</entry>
</row>
</tbody>
</tgroup>
</table>


# computers in een netwerk tellen 
 Hoeveel computers kan je zetten in een netwerk? Houd rekening met de `network id`  network id   die zelf al een ip-adres gebruikt. Elk netwerk heeft ook een `broadcast`  broadcast   adres (alle decimale delen van het host id zijn dan 255). 
 Bijvoorbeeld: 

<table frame='all'>## aantal computers in een subnet  subnet mask   
<?dbfo table-width="100%" ?>
<tgroup cols='4' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="7*" align="center"/>
<colspec colname='c2' colwidth="6*" align="center"/>
<colspec colname='c3' colwidth="7*" align="center"/>
<colspec colname='c4' colwidth="13*" align="center"/>
<thead>
<row>
  <entry>network</entry>
  <entry>network id</entry>
  <entry>broadcast ip</entry>
  <entry>max aantal hosts</entry>
</row>
</thead>
<tbody>
<row>
  <entry>192.168.1.0/24</entry>
  <entry>192.168.1.0</entry>
  <entry>192.168.1.255</entry>
  <entry>256 - 2 = 254</entry>
</row>
<row>
  <entry>192.168.15.0/24</entry>
  <entry>192.168.15.0</entry>
  <entry>192.168.15.255</entry>
  <entry>256 - 2 = 254</entry>
</row>
<row>
  <entry>172.16.0.0/16</entry>
  <entry>172.16.0.0</entry>
  <entry>172.16.255.255</entry>
  <entry>256*256 - 2 = 65534</entry>
</row>
<row>
  <entry>10.0.0.0/8</entry>
  <entry>10.0.0.0</entry>
  <entry>10.255.255.255</entry>
  <entry>256*256*256 - 2 = 16777214</entry>
</row>
</tbody>
</tgroup>
</table>


---

# te weinig ip-adressen ? 
	 Is vier miljard dan niet genoeg ? We verspillen enorm veel ip-adressen door de verkoop van klasse A en klasse B aan organisaties die eigenlijk veel minder adressen nodig hebben. 
	 Wat doe je als je 300 adressen nodig hebt ? Of 2000 ? Je kan kiezen voor een klasse B  class B   range, maar dan verspil je meer dan 90 procent. Je kan ook zeggen dat 8 klasse C  class C   adressen voldoen voor 2000 computers, maar dan vergroot je de `routing tables` weer. 
# ip-adressen verdelen 
 `Klasse A`  class A   adressen kunnen dus een dikke 16 miljoen computers bevatten, `klasse B`  class B   een goeie 65 duizend en `klasse C`  class C   iets meer dan 200. 
 Stel dat jouw organisatie 3000 ip-adressen nodig heeft, dan moet je een klasse B gebruiken. Maar dat wil zeggen dat je meer dan 62 duizend ip-adressen `verspilt`. Onthou ook dat heel wat klasse A ranges begin jaren 90 in hun geheel verkocht zijn. 
 In de jaren 70-80 was dit een goed systeem, er waren immers maar enkele duizenden computers op dit `internetwerk`  internet  . Maar de laatste jaren zijn er meer dan een miljard computers verbonden met internet. Met een totaal van vier miljard (256*256*256*256) ip-adressen kunnen we ons niet langer veroorloven om ip-adressen te verspillen. 


# probleem voor de routing tables 
 Je zou kunnen argumenteren op het vorige dat je ook twaalf `klasse C`  class C   kan gebruiken voor een netwerk met 3000 computers. En dat is correct, maar heeft wel tot gevolg dat de `routing tables`  router   in de internet routers er twaalf routes bij krijgen i.p.v. slechts eentje. 
 Alle grote netwerken opbouwen met klasse C ip ranges is dus ook geen oplosssing. (routing tables bespreken we later) 


# Is nat een oplossing ? 
 Een andere verzachtende techniek voor het naderende tekort aan ip-adressen is `nat`  nat  . Een `nat` toestel kan meerdere `private ip-adressen `  private ip-adressen  (en dus meerdere computers) met een (of enkele) publieke adressen verbinden met het internet. 
 Maar `nat` heeft dan weer het nadeel dat niet alle applicaties kunnen werken achter een `nat`. Bijvoorbeeld `ipsec`  ipsec   (want de poort-informatie is versleuteld), `sip`  sip   (Voice over IP), `ftp`  ftp  , `dns zone transfers`  dns   (we bespreken dit later), `dhcp`, `snmp` en sommige multiplayer spelletjes op Microsofts xbox werken niet over `nat`. 





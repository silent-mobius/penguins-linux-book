---

# solution: control operators 
0. Each question can be answered by one command line! 
1. When you type `passwd`, which file is executed ? 
```sh
which passwd```
2. What kind of file is that ? 
```sh
file /usr/bin/passwd```
3. Execute the `pwd` command twice. (remember 0.) 
```sh
pwd ; pwd```
4. Execute `ls` after `cd /etc`, but only if `cd /etc` did not error. 
```sh
cd /etc &#038;&#038; ls```
5. Execute `cd /etc` after `cd etc`, but only if `cd etc` fails. 
```sh
cd etc || cd /etc```
6. Echo `it worked` when `touch test42` works, and echo `it failed` when the `touch` failed. All on one command line as a normal user (not root). Test this line in your home directory and in `/bin/` . 
```sh
paul@deb106:~$ cd ; touch test42 &#038;&#038; echo it worked || echo it failed
it worked
paul@deb106:~$ cd /bin; touch test42 &#038;&#038; echo it worked || echo it failed
touch: cannot touch `test42': Permission denied
it failed```
7. Execute `sleep 6`, what is this command doing ? 
```sh
pausing for six seconds```
8. Execute `sleep 200` in background (do not wait for it to finish). 
```sh
sleep 200 &#038;```
9. Write a command line that executes `rm file55`. Your command line should print 'success' if file55 is removed, and print 'failed' if there was a problem. 
```sh
rm file55 &#038;&#038; echo success || echo failed```
(optional)10. Use echo to display "Hello World with strange' characters \ * [ }  ~ \\ ." (including all quotes) 
```sh
echo \"Hello World with strange\' characters \\ \* \[ \} \~ \\\\ \. \"```
```sh
or```
```sh
echo \""Hello World with strange' characters \ * [ } ~ \\ . "\"```



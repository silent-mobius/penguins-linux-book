---

# system profile 
Both the `bash`  /bin/bash   and the `ksh`  /bin/ksh   shell will verify the existence of `/etc/profile`  /etc/profile   and `source` it if it exists. 
When reading this script, you will notice (both on Debian and on Red Hat Enterprise Linux) that it builds the PATH environment variable (among others). The script might also change the PS1 variable, set the HOSTNAME and execute even more scripts like `/etc/inputrc`  /etc/inputrc   
This screenshot uses grep to show PATH manipulation in `/etc/profile` on Debian. 
```sh
root@debian10:~# `grep PATH /etc/profile`
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
PATH="/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games"
export PATH
root@debian10:~#```
This screenshot uses grep to show PATH manipulation in `/etc/profile` on RHEL8/CentOS8. 
```sh
[root@centos8 ~]# `grep PATH /etc/profile`
case ":${PATH}:" in
    PATH=$PATH:$1
    PATH=$1:$PATH
export PATH USER LOGNAME MAIL HOSTNAME HISTSIZE HISTCONTROL
[root@centos8 ~]#```
The `root user` can use this script to set aliases, functions, and variables for every user on the system. 


# ~/.bash_profile 
When this file exists in the home directory, then `bash` will source it. On Debian Linux 8/9/10 this file does not exist by default. 
RHEL8/CentOS8 uses a small `~/.bash_profile`  .bash_profile   where it checks for the existence of `~/.bashrc`  .bashrc   and then sources it. It also adds $HOME/bin to the $PATH variable. 
```sh
[root@rhel8 ~]# `cat /home/paul/.bash_profile`
# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH
[root@rhel8 ~]#```


---

# ~/.bash_login 
When `.bash_profile` does not exist, then `bash` will check for `~/.bash_login`  .bash_login   and source it. 
Neither Debian nor Red Hat have this file by default. 


# ~/.profile 
When neither `~/.bash_profile` and `~/.bash_login` exist, then bash will verify the existence of `~/.profile` and execute it. This file does not exist by default on Red Hat. 
On Debian this script can execute `~/.bashrc`  .bashrc   and will add $HOME/bin to the $PATH variable. 
```sh
root@debian10:~# `tail -11 /home/paul/.profile`
if [ -n "$BASH_VERSION" ]; then
# include .bashrc if it exists
if [ -f "$HOME/.bashrc" ]; then
. "$HOME/.bashrc"
fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
PATH="$HOME/bin:$PATH"
fi```
RHEL/CentOS does not have this file by default. 


# ~/.bashrc 
The `~/.bashrc` script is often sourced by other scripts. Let us take a look at what it does by default. 
Red Hat uses a very simple `~/.bashrc`, checking for `/etc/bashrc`  /etc/bashrc   and sourcing it. It also leaves room for custom aliases and functions. 
```sh
[root@rhel8 ~]# `cat /home/paul/.bashrc`
# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions```
On Debian this script is quite a bit longer and configures $PS1, some history variables and a number af active and inactive aliases. 
```sh
root@debian10:~# wc -l /home/paul/.bashrc
110 /home/paul/.bashrc```


---

# ~/.bash_logout 
When exiting `bash`, it can execute `~/.bash_logout`  .bash_logout  . 
Debian use this opportunity to clear the console screen. 
```sh
serena@deb106:~$ `cat .bash_logout`
# ~/.bash_logout: executed by bash(1) when login shell exits.

# when leaving the console clear the screen to increase privacy

if [ "$SHLVL" = 1 ]; then
[ -x /usr/bin/clear_console ] &#038;&#038; /usr/bin/clear_console -q
fi```
Red Hat Enterprise Linux 5 will simple call the `/usr/bin/clear` command in this script. 
```sh
[serena@rhel53 ~]$ cat .bash_logout 
# ~/.bash_logout

/usr/bin/clear```
Red Hat Enterprise Linux 6,7 and 8 create this file, but leave it empty (except for a comment). 
```sh
paul@rhel65:~$ `cat .bash_logout`
# ~/.bash_logout```


---

# Debian overview 
Below is a table overview of when Debian is running any of these bash startup scripts. 
<table frame='all'>## Debian User Environment 
<?dbfo table-width="60%" ?>
<tgroup cols='5' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="3*" align="left"/>
<colspec colname='c2' colwidth="1*" align="center"/>
<colspec colname='c3' colwidth="1*" align="center"/>
<colspec colname='c4' colwidth="1*" align="center"/>
<colspec colname='c5' colwidth="1*" align="center"/>
<thead>
<row>
<entry>script</entry>
<entry>su</entry>
<entry>su -</entry>
<entry>ssh</entry>
<entry>gdm</entry>
</row>
</thead>
<tbody>
<row>
<entry>~./bashrc</entry>
<entry>no</entry>
<entry>yes</entry>
<entry>yes</entry>
<entry>yes</entry>
</row>
<row>
<entry>~/.profile</entry>
<entry>no</entry>
<entry>yes</entry>
<entry>yes</entry>
<entry>yes</entry>
</row>
<row>
<entry>/etc/profile</entry>
<entry>no</entry>
<entry>yes</entry>
<entry>yes</entry>
<entry>yes</entry>
</row>
<row>
<entry>/etc/bash.bashrc</entry>
<entry>yes</entry>
<entry>no</entry>
<entry>no</entry>
<entry>yes</entry>
</row>
</tbody>
</tgroup>
</table>


# RHEL6 overview 
Below is a table overview of when Red Hat Enterprise Linux 5 is running any of these bash startup scripts. 
<table frame='all'>## Red Hat User Environment 
<?dbfo table-width="60%" ?>
<tgroup cols='5' align='left' colsep='1' rowsep='1'>
<colspec colname='c1' colwidth="3*" align="left"/>
<colspec colname='c2' colwidth="1*" align="center"/>
<colspec colname='c3' colwidth="1*" align="center"/>
<colspec colname='c4' colwidth="1*" align="center"/>
<colspec colname='c5' colwidth="1*" align="center"/>
<thead>
<row>
<entry>script</entry>
<entry>su</entry>
<entry>su -</entry>
<entry>ssh</entry>
<entry>gdm</entry>
</row>
</thead>
<tbody>
<row>
<entry>~./bashrc</entry>
<entry>yes</entry>
<entry>yes</entry>
<entry>yes</entry>
<entry>yes</entry>
</row>
<row>
<entry>~/.bash_profile</entry>
<entry>no</entry>
<entry>yes</entry>
<entry>yes</entry>
<entry>yes</entry>
</row>
<row>
<entry>/etc/profile</entry>
<entry>no</entry>
<entry>yes</entry>
<entry>yes</entry>
<entry>yes</entry>
</row>
<row>
<entry>/etc/bashrc</entry>
<entry>yes</entry>
<entry>yes</entry>
<entry>yes</entry>
<entry>yes</entry>
</row>
</tbody>
</tgroup>
</table>



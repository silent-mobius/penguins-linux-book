---

# solution: read only file server 
1. Create a directory in a good location (FHS) to share files for everyone to read. 
```sh
choose one of these...```
```sh
mkdir -p /srv/samba/readonly```
```sh
mkdir -p /home/samba/readonly```
```sh
/home/paul/readonly is wrong!!```
```sh
/etc/samba/readonly is wrong!!```
```sh
/readonly is wrong!!```
2. Make sure the directory is owned properly and is world accessible. 
```sh
chown root:root /srv/samba/readonly```
```sh
chmod 755 /srv/samba/readonly```
3. Put a textfile in this directory. 
```sh
echo Hello World > hello.txt```
4. Share the directory with Samba. 
```sh

You smb.conf.readonly could look like this:
[global]
workgroup = WORKGROUP
server string = Read Only File Server
netbios name = STUDENTx
security = share

[readonlyX]
path = /srv/samba/readonly
comment = read only file share
read only = yes
guest ok = yes
```
```sh
test with testparm before going in production!```
5. Verify from your own and from another computer (smbclient, net use, ...) that the share is accessible for reading. 
```sh
On Linux: smbclient -NL 127.0.0.1```
```sh
On Windows Explorer: browse to My Network Places```
```sh
On Windows cmd.exe: net use L: //studentx/readonly```
6. Make a backup copy of your smb.conf, name it smb.conf.ReadOnlyFileServer. 
```sh
cp smb.conf smb.conf.ReadOnlyFileServer```



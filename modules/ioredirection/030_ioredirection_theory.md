---

# stdin, stdout, and stderr 
The bash shell has three basic streams; it takes input from `stdin` (stream `0`)  stdin  , it sends output to `stdout` (stream `1`)   stdout   and it sends error messages to `stderr` (stream `2`)   stderr  . 
The drawing below has a graphical interpretation of these three streams. 
<img  src="img/bash_stdin_stdout_stderr.svg" format="SVG" align="center" width="14cm"> </img>  
The keyboard often serves as `stdin`, whereas `stdout` and `stderr` both go to the display. This can be confusing to new Linux users because there is no obvious way to recognize `stdout` from `stderr`. Experienced users know that separating output from errors can be very useful. 
<img  src="img/bash_ioredirection_keyboard_display.png" format="PNG" align="center"> </img>  
The next sections will explain how to redirect these streams. 


---

# output redirection 
# &#062; stdout 
`stdout` can be redirected with a `greater than` sign. While scanning the line, the shell will see the `&#062;`  &#062;   sign and will clear the file. 
<img  src="img/bash_output_redirection.png" format="PNG" align="center"> </img>  
The `&#062;` notation is in fact the abbreviation of `1&#062;` (`stdout` being referred to as stream `1`). 
```sh
[paul@RHELv8u3 ~]$ echo It is cold today!
It is cold today!
[paul@RHELv8u3 ~]$ echo It is cold today! &#062; winter.txt
[paul@RHELv8u3 ~]$ cat winter.txt 
It is cold today!
[paul@RHELv8u3 ~]$```
Note that the bash shell effectively `removes` the redirection from the command line before argument 0 is executed. This means that in the case of this command: 
```sh
echo hello > greetings.txt```
the shell only counts two arguments (echo = argument 0, hello = argument 1). The redirection is removed before the argument counting takes place. 


# output file is erased 
While scanning the line, the shell will see the &#062; sign and `will clear the file`! Since this happens before resolving `argument 0`, this means that even when the command fails, the file will have been cleared! 
```sh
[paul@RHELv8u3 ~]$ cat winter.txt 
It is cold today!
[paul@RHELv8u3 ~]$ zcho It is cold today! &#062; winter.txt
-bash: zcho: command not found
[paul@RHELv8u3 ~]$ cat winter.txt 
[paul@RHELv8u3 ~]$```


---

# noclobber 
Erasing a file while using &#062; can be prevented by setting the `noclobber`  noclobber   option. 
```sh
[paul@RHELv8u3 ~]$ cat winter.txt 
It is cold today!
[paul@RHELv8u3 ~]$ set -o noclobber
[paul@RHELv8u3 ~]$ echo It is cold today! &#062; winter.txt
-bash: winter.txt: cannot overwrite existing file
[paul@RHELv8u3 ~]$ set +o noclobber
[paul@RHELv8u3 ~]$```


# overruling noclobber 
The `noclobber` can be overruled with `&#062;|`  &#062;|  . 
```sh
[paul@RHELv8u3 ~]$ set -o noclobber
[paul@RHELv8u3 ~]$ echo It is cold today! &#062; winter.txt
-bash: winter.txt: cannot overwrite existing file
[paul@RHELv8u3 ~]$ echo It is very cold today! &#062;| winter.txt
[paul@RHELv8u3 ~]$ cat winter.txt 
It is very cold today!
[paul@RHELv8u3 ~]$```


# &#062;&#062; append 
Use `&#062;&#062;`  &#062;&#062;   to `append` output to a file. 
```sh
[paul@RHELv8u3 ~]$ echo It is cold today! &#062; winter.txt
[paul@RHELv8u3 ~]$ cat winter.txt 
It is cold today!
[paul@RHELv8u3 ~]$ echo Where is the summer ? &#062;&#062; winter.txt
[paul@RHELv8u3 ~]$ cat winter.txt 
It is cold today!
Where is the summer ?
[paul@RHELv8u3 ~]$```




---

# error redirection 
# 2&#062; stderr 
Redirecting `stderr` is done with `2&#062;`  2&#062;  . This can be very useful to prevent error messages from cluttering your screen. 
<img  src="img/bash_error_redirection.png" format="PNG" align="center"> </img>  
The screenshot below shows redirection of `stdout` to a file, and `stderr` to `/dev/null`  /dev/null  . Writing `1&#062;`  1&#062;   is the same as &#062;. 
```sh
[paul@RHELv8u3 ~]$ find / &#062; allfiles.txt 2&#062; /dev/null
[paul@RHELv8u3 ~]$```


# 2&#062;&#038;1 
To redirect both `stdout` and `stderr` to the same file, use `2>&#038;1`  2>&#038;1  . 
```sh
[paul@RHELv8u3 ~]$ find / &#062; allfiles_and_errors.txt 2&#062;&#038;1
[paul@RHELv8u3 ~]$```
Note that the order of redirections is significant. For example, the command 
```sh
ls > dirlist 2>&amp;1```
directs both standard output (file descriptor 1) and standard error (file descriptor 2) to the file dirlist, while the command 
```sh
ls 2>&amp;1 > dirlist```
directs only the standard output to file dirlist, because the standard error made a copy of the standard output before the standard output was redirected to dirlist.  




---

# output redirection and pipes 
By default you cannot grep inside `stderr` when using pipes on the command line, because only `stdout` is passed. 
```sh
paul@debian10:~$ rm file42 file33 file1201 | grep file42
rm: cannot remove ‘file42’: No such file or directory
rm: cannot remove ‘file33’: No such file or directory
rm: cannot remove ‘file1201’: No such file or directory```
With `2&#062;&#038;1` you can force `stderr` to go to `stdout`. This enables the next command in the pipe to act on both streams. 
```sh
paul@debian10:~$ rm file42 file33 file1201 2>&#038;1 | grep file42
rm: cannot remove ‘file42’: No such file or directory```
You cannot use both `1&#062;&#038;2` and `2&#062;&#038;1` to switch `stdout` and `stderr`. 
```sh
paul@debian10:~$ rm file42 file33 file1201 2>&#038;1 1>&#038;2 | grep file42
rm: cannot remove ‘file42’: No such file or directory
paul@debian10:~$ echo file42 2>&#038;1 1>&#038;2 | sed 's/file42/FILE42/' 
FILE42```
You need a third stream to switch stdout and stderr after a pipe symbol. 
```sh
paul@debian10:~$ echo file42 3>&#038;1 1>&#038;2 2>&#038;3 | sed 's/file42/FILE42/' 
file42
paul@debian10:~$ rm file42 3>&#038;1 1>&#038;2 2>&#038;3 | sed 's/file42/FILE42/' 
rm: cannot remove ‘FILE42’: No such file or directory```


# joining stdout and stderr 
The `&#038;>` construction will put both `stdout` and `stderr` in one stream (to a file). 
```sh
paul@debian10:~$ rm file42 &#038;> out_and_err
paul@debian10:~$ cat out_and_err 
rm: cannot remove ‘file42’: No such file or directory
paul@debian10:~$ echo file42 &#038;> out_and_err
paul@debian10:~$ cat out_and_err 
file42
paul@debian10:~$ ```


---

# input redirection 
# &#060; stdin 
Redirecting `stdin` is done with &#060; (short for 0&#060;). 
```sh
[paul@RHEL8b ~]$ cat &#060; text.txt
one
two
[paul@RHEL8b ~]$ tr 'onetw' 'ONEZZ' &#060; text.txt
ONE
ZZO
[paul@RHEL8b ~]$```


# &#060;&#060; here document 
The `here document`  here document   (sometimes called here-is-document) is a way to append input until a certain sequence (usually EOF) is encountered. The `EOF`  EOF   marker can be typed literally or can be called with Ctrl-D. 
```sh

[paul@RHEL8b ~]$ cat &#060;&#060;EOF &#062; text.txt
> one
> two
> EOF
[paul@RHEL8b ~]$ cat text.txt 
one
two
[paul@RHEL8b ~]$ cat &#060;&#060;brol &#062; text.txt
> brel
> brol
[paul@RHEL8b ~]$ cat text.txt 
brel
[paul@RHEL8b ~]$
```


# &#060;&#060;&#060; here string 
The `here string`  here string   can be used to directly pass strings to a command. The result is the same as using `echo string | command` (but you have one less process running). 
```sh
paul@ubu1110~$ base64 &#060;&#060;&#060; linux-training.be
bGludXgtdHJhaW5pbmcuYmUK
paul@ubu1110~$ base64 -d &#060;&#060;&#060; bGludXgtdHJhaW5pbmcuYmUK
linux-training.be```
See rfc 3548 for more information about `base64`  base64  . 




---

# confusing redirection 
The shell will scan the whole line before applying redirection. The following command line is very readable and is correct. 
```sh
cat winter.txt &#062; snow.txt 2&#062; errors.txt```
But this one is also correct, but less readable. 
```sh
2&#062; errors.txt cat winter.txt &#062; snow.txt```
Even this will be understood perfectly by the shell. 
```sh
&#060; winter.txt &#062; snow.txt 2&#062; errors.txt cat```


# quick file clear 
So what is the quickest way to clear a file ? 
```sh
&#062;foo```
And what is the quickest way to clear a file when the `noclobber` option is set ? 
```sh
&#062;|bar```



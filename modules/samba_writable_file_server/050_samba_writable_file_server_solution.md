---

# solution: writable file server 
1. Create a directory and share it with Samba. 
```sh
mkdir /srv/samba/writable```
```sh
chmod 777 /srv/samba/writable```
```sh
```
```sh
the share section in smb.conf can look like this:```
```sh
[pubwrite]
path = /srv/samba/writable
comment = files to write
read only = no
guest ok = yes```
2. Make sure everyone can read and write files, test writing with smbclient and from a Microsoft computer. 
```sh
to test writing with smbclient:```
```sh

echo one > count.txt
echo two >> count.txt
echo three >> count.txt
smbclient //localhost/pubwrite
Password: 
smb: \> put count.txt
```
3. Verify the ownership of files created by (various) users. 
```sh
ls -l /srv/samba/writable```



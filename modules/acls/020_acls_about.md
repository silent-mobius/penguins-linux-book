
---
## About ACL

Standard Unix permissions might not be enough for some organizations. This chapter introduces `access control lists` or `acl`s to further protect files and directories.


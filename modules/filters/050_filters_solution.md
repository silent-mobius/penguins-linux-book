---

# solution: filters 
1. Put a sorted list of all bash users in bashusers.txt. 
```sh
grep bash /etc/passwd | cut -d: -f1 | sort &#062; bashusers.txt```
2. Put a sorted list of all logged on users in onlineusers.txt. 
```sh
who | cut -d' ' -f1 | sort &#062; onlineusers.txt```
3. Make a list of all filenames in `/etc` that contain the string `conf` in their filename. 
```sh
ls /etc | grep conf```
4. Make a sorted list of all files in `/etc` that contain the case insensitive string `conf` in their filename. 
```sh
ls /etc | grep -i conf | sort```
5. Look at the output of `/sbin/ifconfig`. Write a line that displays only ip address and the subnet mask. 
```sh
/sbin/ifconfig | head -2 | grep 'inet ' | tr -s ' ' | cut -d' ' -f3,5```
6. Write a line that removes all non-letters from a stream. 
```sh
paul@deb106:~$ cat text
This is, yes really! , a text with ?&#038;* too many str$ange# characters ;-)
paul@deb106:~$ cat text | tr -d ',!$?.*&#038;^%#@;()-'
This is yes really  a text with  too many strange characters
```
7. Write a line that receives a text file, and outputs all words on a separate line. 
```sh

paul@deb106:~$ cat text2 
it is very cold today without the sun

paul@deb106:~$ cat text2 | tr ' ' '\n'
it
is
very
cold
today
without
the
sun
```
8. Write a spell checker on the command line. (There may be a dictionary in `/usr/share/dict/` .) 
```sh

paul@rhel ~$ echo "The zun is shining today" > text

paul@rhel ~$ cat > DICT
is
shining
sun
the
today

paul@rhel ~$ cat text | tr 'A-Z ' 'a-z\n' | sort | uniq | comm -23 - DICT
zun
```
You could also add the solution from question number 6 to remove non-letters, and `tr -s ' '` to remove redundant spaces. 



#!/usr/bin/env python3
#
###############################################
#
#
#
#
###############################################
import os
import sys
import glob
import argparse
import pathlib
import logging
from markdown import markdown
import pdfkit

input_filename = 'build.md'
output_filename = 'build.pdf'
output_folder = '../output'
out = output_folder+'/'+output_filename

options={'page-size': 'A4',
         'margin-top': '10',
         'margin-right': '30',
         'margin-left': '30',
         'margin-bottom': '10',
         'zoom': '1.2',
         'encoding': "UTF-8",
         'enable-local-file-access': ""
         }

css='book.css'




def get_path(path):
    root = os.path.join(os.getcwd(),'../modules/{}'.format(path))
    files = os.listdir(root)
    files.sort()
    full_path_files=[]
    for f in files:
        logging.info('adding {} to list'.format(root+'/'+f))
        full_path_files.append(root+'/'+f)
    return full_path_files

def clean_up_output_folder(path):
    root = os.path.join(os.getcwd(),'../output/{}'.format(path))
    files = os.listdir(root)
    try:
        for f in files:
            logging.info('removing file {} from directory'.format(root+'/'+f))
            os.remove(root+'/'+f)
        return True
    except Exception as e:
        logging.error('Error Happened: ',e)
        return False

file_content_list=get_path('systemd')
file_content_list.sort()

if not os._exists(output_folder):
    os.mkdir(output_folder)
elif os._exists(out_folder) and len(os.listdir(output_folder)) != 0:
    clean_up_output_folder

with open(input_filename, "wb") as outfile:
    for f in file_content_list:
        with open(f, "rb") as infile:
            outfile.write(infile.read())


with open('build.md', 'r') as f:
    html_text = markdown(f.read(), extensions=['fenced_code','codehilite', 'extra', 'toc', 'smarty', 'sane_lists', 'meta'], output_format='html4')
    # print(html_texta)

pdfkit.from_string(html_text,output_filename, options=options, css=css, verbose=True)


